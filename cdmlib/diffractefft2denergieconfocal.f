c     Cette fonction calcul le champ diffracté sur un plan, d'ou la
c     correction en -2i pi gamma.
      subroutine diffractefft2denergieconfocal(speckseed,numaperil,nx,ny
     $     ,nz,nxm,nym,nzm,nfft2d,tabfft2,k0,xs,ys,zs,E0,ss,pp,theta
     $     ,phi,xgaus,ygaus,zgaus ,w0 ,aretecube,Eloinx,Eloiny ,Eloinz
     $     ,FF,imax,deltakx,deltaky ,Ediffkzpos,Ediffkzneg,beam
     $     ,efficacite,efficaciteref ,efficacitetrans,nsectionsca
     $     ,nquickdiffracte,plan2f,plan2b,nside ,nstop ,infostr)
      implicit none
      integer nx,ny,nz,nxm,nym,nzm,nfft2d,nstop,nsectionsca
     $     ,nquickdiffracte,iref,speckseed,iref1,nside
      double precision xs(nxm*nym*nzm),ys(nxm*nym*nzm),zs(nxm*nym*nzm)
     $     ,aretecube,k0,k02,tmp,const1,kmax,kp2,psi,x0,y0,z0,numaperil
      double complex FF(3*nxm*nym*nzm),Ediffkzpos(nfft2d,nfft2d,3)
     $     ,Ediffkzneg(nfft2d,nfft2d,3),const2,const3

      integer nfft2d2,imax,i,j,k,tabfft2(nfft2d),indice,kk,ii,jj
      double precision deltakx,deltaky,var1,var2,kx,ky,kz,fac,pi
      double complex ctmp,ctmp1,icomp,Eloinx(nfft2d*nfft2d)
     $     ,Eloiny(nfft2d*nfft2d),Eloinz(nfft2d*nfft2d)
      character(64) infostr

c     variable pour champ incident
      integer nloin,indicex,indicey
      double precision ss,pp,theta,phi,xdip ,ydip,zdip,xgaus,ygaus
     $     ,zgaus,w0,x,y,z,tol,deltax,deltay,tolc,Emod,Emodmax
      double complex E0,Em(3),Axp ,Ayp ,Azp ,Axs,Ays,Azs,Ax,Ay,Az,Axr
     $     ,Ayr,Azr
      character(64) beam
      integer nbinc
      double precision thetam(10), phim(10), ppm(10), ssm(10)
      double complex E0m(10)
c     energie
      double precision fluxinc,fluxref,fluxtra,efficacite,efficaciteref
     $     ,efficacitetrans
      double precision tmp1,tmp2,ran,sunif,ran1
      integer*8 plan2f,plan2b
      integer FFTW_FORWARD
      FFTW_FORWARD=-1    

      pi=dacos(-1.d0)
      icomp=(0.d0,1.d0)
      deltakx=2.d0*pi/(dble(nfft2d)*aretecube)
      deltaky=deltakx
      nfft2d2=nfft2d/2  
      var1=(xs(1)+dble(nfft2d2)*aretecube)*deltakx
      var2=(ys(1)+dble(nfft2d2)*aretecube)*deltaky
      k02=k0*k0
      psi=pp*pi/180.d0
      nfft2d2=nfft2d/2
      x0=xgaus
      y0=ygaus
      z0=zgaus
      kmax=k0*numaperil
      write(*,*) 'delta k',deltakx,'m-1'
      if (nfft2d.gt.16384) then
         nstop=1
         infostr='nfft2d too large'
         return
      endif
      
      if (deltakx.ge.k0) then
         nstop=1
         infostr='In FFT Poynting nfft2d too small'
         return
      endif
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
      do i=1,nfft2d
         if (i-nfft2d2-1.ge.0) then
            tabfft2(i)=i-nfft2d2
         else
            tabfft2(i)=nfft2d2+i
         endif
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      imax=nint(k0/deltakx)+1
      write(*,*) 'Number of point in NA     :',imax*2+1
      write(*,*) 'Size of the FFT           :',nfft2d
      write(*,*) 'Magnitude of initial field:',E0
      if (2*imax+1.gt.nfft2d) then
         infostr='In FFT diffract nfft2d too small'
         nstop = 1
         return
      endif
      if (2*imax+1.lt.7) then
         infostr='In FFT diffract nfft2d too small'
         nstop = 1
         return
      endif

      if (nsectionsca*nquickdiffracte.eq.0) then
         
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j) 
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)          
         do i=1,nfft2d
            do j=1,nfft2d  
               Ediffkzneg(i,j,1)=0.d0
               Ediffkzneg(i,j,2)=0.d0
               Ediffkzneg(i,j,3)=0.d0
               Ediffkzpos(i,j,1)=0.d0
               Ediffkzpos(i,j,2)=0.d0
               Ediffkzpos(i,j,3)=0.d0
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL   

         
         do k=1,nz
            
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)           
            do i=1,nfft2d*nfft2d
               Eloinx(i)=0.d0
               Eloiny(i)=0.d0
               Eloinz(i)=0.d0
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL            

            
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,kk,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2) 
            do i=1,nx
               do j=1,ny
                  kk=i+nx*(j-1)+nx*ny*(k-1)
                  indice=tabfft2(i)+nfft2d*(tabfft2(j)-1)
                  Eloinx(indice)=FF(3*(kk-1)+1)                
                  Eloiny(indice)=FF(3*(kk-1)+2)    
                  Eloinz(indice)=FF(3*(kk-1)+3)  
               enddo
            enddo
            
!$OMP ENDDO 
!$OMP END PARALLEL     

#ifdef USE_FFTW
            call dfftw_execute_dft(plan2f,Eloinx,Eloinx)
            call dfftw_execute_dft(plan2f,Eloiny,Eloiny)
            call dfftw_execute_dft(plan2f,Eloinz,Eloinz)
#else
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP SECTIONS 
!$OMP SECTION   
            call fftsingletonz2d(Eloinx,nfft2d,nfft2d,FFTW_FORWARD)
!$OMP SECTION   
            call fftsingletonz2d(Eloiny,nfft2d,nfft2d,FFTW_FORWARD)
!$OMP SECTION   
            call fftsingletonz2d(Eloinz,nfft2d,nfft2d,FFTW_FORWARD)
!$OMP END SECTIONS
!$OMP END PARALLEL

#endif
         
            kk=1+nx*ny*(k-1)

!$OMP PARALLEL DEFAULT(SHARED)
!$OMP& PRIVATE(i,j,ii,jj,kx,ky,kz,indice,ctmp,ctmp1)
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)             
            do i=-imax,imax
               do j=-imax,imax
                  ii=imax+i+1
                  jj=imax+j+1
                  kx=dble(i)*deltakx
                  ky=dble(j)*deltaky
                  if (k0*k0-kx*kx-ky*ky.gt.0.d0) then

                     kz=dsqrt(k0*k0-kx*kx-ky*ky) 
                     
                     indice=tabfft2(i+nfft2d2+1)+nfft2d*(tabfft2(j
     $                    +nfft2d2+1)-1)

                     ctmp=(kx*Eloinx(indice)+ky*Eloiny(indice)+kz
     $                    *Eloinz(indice))/k0
                     ctmp1=cdexp(-icomp*kz*zs(kk))*cdexp(-icomp*(var1
     $                    *dble(i)+var2*dble(j)))

                     Ediffkzpos(ii,jj,1)=Ediffkzpos(ii,jj,1)
     $                    +(Eloinx(indice)-kx*ctmp/k0)*ctmp1
                     Ediffkzpos(ii,jj,2)=Ediffkzpos(ii,jj,2)
     $                    +(Eloiny(indice)-ky*ctmp/k0)*ctmp1
                     Ediffkzpos(ii,jj,3)=Ediffkzpos(ii,jj,3)
     $                    +(Eloinz(indice)-kz*ctmp/k0)*ctmp1

                     ctmp=(kx*Eloinx(indice)+ky*Eloiny(indice)-kz
     $                    *Eloinz(indice))/k0
                     ctmp1=cdexp(icomp*kz*zs(kk))*cdexp(-icomp*(var1
     $                    *dble(i)+var2*dble(j)))

                     Ediffkzneg(ii,jj,1) =Ediffkzneg(ii,jj,1)
     $                    +(Eloinx(indice)-kx*ctmp/k0)*ctmp1
                     Ediffkzneg(ii,jj,2) =Ediffkzneg(ii,jj,2)
     $                    +(Eloiny(indice)-ky*ctmp/k0)*ctmp1
                     Ediffkzneg(ii,jj,3) =Ediffkzneg(ii,jj,3)
     $                    +(Eloinz(indice)+kz*ctmp/k0)*ctmp1
                     
                  endif
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         enddo 
      endif


!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,kx,ky,kz,ctmp,ii,jj)
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)
      do i=-imax,imax
         do j=-imax,imax
            kx=dble(i)*deltakx
            ky=dble(j)*deltaky
            if (k0*k0-kx*kx-ky*ky.gt.0.d0) then               
               kz=dsqrt(k0*k0-kx*kx-ky*ky)
               ctmp=-2.d0*pi*icomp*kz
               ii=imax+i+1
               jj=imax+j+1
               Ediffkzpos(ii,jj,1)=Ediffkzpos(ii,jj,1)*k02/ctmp
               Ediffkzpos(ii,jj,2)=Ediffkzpos(ii,jj,2)*k02/ctmp
               Ediffkzpos(ii,jj,3)=Ediffkzpos(ii,jj,3)*k02/ctmp
               Ediffkzneg(ii,jj,1)=Ediffkzneg(ii,jj,1)*k02/ctmp
               Ediffkzneg(ii,jj,2)=Ediffkzneg(ii,jj,2)*k02/ctmp
               Ediffkzneg(ii,jj,3)=Ediffkzneg(ii,jj,3)*k02/ctmp      
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

c     calcul pour rajouter le champ incident
      deltax=2.d0*pi/(dble(nfft2d)*deltakx)
      deltay=2.d0*pi/(dble(nfft2d)*deltaky)
      write(*,*) 'Size of the pixel     :',deltax,'nm'
      z=0.d0

      
      fluxinc=0.d0
      fluxref=0.d0
      fluxtra=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)
      do i=1,nfft2d*nfft2d
         Eloinx(i)=0.d0
         Eloiny(i)=0.d0
         Eloinz(i)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         
      if (beam(1:11).eq.'demispeckle') then
         iref=4*speckseed+1
         ran=SUNIF(iref)
      
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky

               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then

                  if (i.ge.0) then
                     indicex=i+1
                  else
                     indicex=nfft2d+i+1
                  endif
                  if (j.ge.0) then
                     indicey=j+1
                  else
                     indicey=nfft2d+j+1
                  endif
                  indice=indicex+nfft2d*(indicey-1)
                  
                  ran=SUNIF(-1)
                  kz=dsqrt(k0*k0-kp2)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))*cdexp(icomp
     $                 *2.d0*pi*ran)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp*const2
                  Ay=E0*dsin(psi)*Ays*const2
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const2
c     changement de base pour retourner dans la base de la surface
                  Eloinx(indice)=Ax
                  Eloiny(indice)=Ay
                  Eloinz(indice)=Az
                  fluxinc=fluxinc+(dreal(Ax*dconjg(Ax)) +dreal(Ay
     $                 *dconjg(Ay))+dreal(Az*dconjg(Az)))*kz              
                  fluxref=fluxref+(dreal(Ediffkzneg(ii,jj,1)
     $                 *dconjg(Ediffkzneg(ii,jj,1))) +
     $                 dreal(Ediffkzneg(ii ,jj,2) *dconjg(Ediffkzneg(ii
     $                 ,jj,2))) + dreal(Ediffkzneg(ii,jj,3)
     $                 *dconjg(Ediffkzneg(ii,jj ,3))))*kz
                  fluxtra=fluxtra+(dreal((Ediffkzpos(ii,jj,1)+Ax)
     $                 *dconjg(Ediffkzpos(ii,jj,1) +Ax)) +
     $                 dreal((Ediffkzpos(ii,jj,2) +Ay)
     $                 *dconjg(Ediffkzpos(ii,jj,2) +Ay))
     $                 +dreal((Ediffkzpos(ii,jj,3) +Az)
     $                 *dconjg(Ediffkzpos(ii,jj,3) +Az)))*kz
               
               endif
            enddo
         enddo


      elseif (beam(1:12).eq.'demiconfocal') then
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP& PRIVATE(i,j,kx,ky,ii,jj,kz,ran,kp2,const1,const2)
!$OMP& PRIVATE(Axp,Azp,Ays,Azs,Ax,Ay,Az,indicex,indicey,indice)        
!$OMP DO SCHEDULE(DYNAMIC)  COLLAPSE(2)
!$OMP& REDUCTION(+:fluxinc,fluxref,fluxtra)         
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky

               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
                  if (i.ge.0) then
                     indicex=i+1
                  else
                     indicex=nfft2d+i+1
                  endif
                  if (j.ge.0) then
                     indicey=j+1
                  else
                     indicey=nfft2d+j+1
                  endif
                  indice=indicex+nfft2d*(indicey-1)
                  kz=dsqrt(k0*k0-kp2)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp*const2
                  Ay=E0*dsin(psi)*Ays*const2
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const2
                  Eloinx(indice)=Ax
                  Eloiny(indice)=Ay
                  Eloinz(indice)=Az
c     changement de base pour retourner dans la base de la surface
            
                  fluxinc=fluxinc+(dreal(Ax*dconjg(Ax)) +dreal(Ay
     $                 *dconjg(Ay))+dreal(Az*dconjg(Az)))*kz              
                  fluxref=fluxref+(dreal(Ediffkzneg(ii,jj,1)
     $                 *dconjg(Ediffkzneg(ii,jj,1))) +
     $                 dreal(Ediffkzneg(ii ,jj,2) *dconjg(Ediffkzneg(ii
     $                 ,jj,2))) + dreal(Ediffkzneg(ii,jj,3)
     $                 *dconjg(Ediffkzneg(ii,jj ,3))))*kz
                  fluxtra=fluxtra+(dreal((Ediffkzpos(ii,jj,1)+Ax)
     $                 *dconjg(Ediffkzpos(ii,jj,1)+Ax)) +
     $                 dreal((Ediffkzpos(ii,jj,2)+Ay)
     $                 *dconjg(Ediffkzpos(ii,jj,2)+Ay))
     $                 +dreal((Ediffkzpos(ii,jj,3)+Az)
     $                 *dconjg(Ediffkzpos(ii,jj,3)+Az)))*kz
               
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      elseif (beam(1:7).eq.'speckle') then
         iref=4*speckseed+1
         ran=SUNIF(iref)
         iref1=4*speckseed+5
         ran1=SUNIF(iref1)
       
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky

               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
                  if (i.ge.0) then
                     indicex=i+1
                  else
                     indicex=nfft2d+i+1
                  endif
                  if (j.ge.0) then
                     indicey=j+1
                  else
                     indicey=nfft2d+j+1
                  endif
                  indice=indicex+nfft2d*(indicey-1)
                  ran=SUNIF(-1)
                  ran1=SUNIF(-1)                  
c                  ran=0.d0
c                  ran1=0.d0
                  kz=dsqrt(k0*k0-kp2)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))*cdexp(icomp
     $                 *2.d0*pi*ran)
                  const3=cdexp(-icomp*(kx*x0+ky*y0-kz*z0))*cdexp(icomp
     $                 *2.d0*pi*ran1)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp*const2
                  Ay=E0*dsin(psi)*Ays*const2
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const2                
c     changement de base pour retourner dans la base de la surface
                  Axr=E0*dcos(psi)*Axp*const3
                  Ayr=E0*dsin(psi)*Ays*const3
                  Azr=-E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const3
                  if (nside.eq.1) then
                     Eloinx(indice)=Ax
                     Eloiny(indice)=Ay
                     Eloinz(indice)=Az
                  elseif (nside.eq.-1) then
                     Eloinx(indice)=Axr
                     Eloiny(indice)=Ayr
                     Eloinz(indice)=Azr
                  endif
                  fluxinc=fluxinc+(dreal(Ax*dconjg(Ax)) +dreal(Ay
     $                 *dconjg(Ay))+dreal(Az*dconjg(Az)))*kz
                  fluxinc=fluxinc+(dreal(Axr*dconjg(Axr)) +dreal(Ayr
     $                 *dconjg(Ayr))+dreal(Azr*dconjg(Azr)))*kz 
                  fluxref=fluxref+(dreal((Ediffkzneg(ii,jj,1)+Axr)
     $                 *dconjg(Ediffkzneg(ii,jj,1)+Axr)) +
     $                 dreal((Ediffkzneg(ii,jj,2)+Ayr)
     $                 *dconjg(Ediffkzneg(ii,jj,2)+Ayr))
     $                 +dreal((Ediffkzneg(ii,jj,3)+Azr)
     $                 *dconjg(Ediffkzneg(ii,jj,3)+Azr)))*kz
                  fluxtra=fluxtra+(dreal((Ediffkzpos(ii,jj,1)+Ax)
     $                 *dconjg(Ediffkzpos(ii,jj,1) +Ax)) +
     $                 dreal((Ediffkzpos(ii,jj,2) +Ay)
     $                 *dconjg(Ediffkzpos(ii,jj,2) +Ay))
     $                 +dreal((Ediffkzpos(ii,jj,3) +Az)
     $                 *dconjg(Ediffkzpos(ii,jj,3) +Az)))*kz
               endif
            enddo
         enddo

      elseif (beam(1:8).eq.'confocal') then         

!$OMP PARALLEL DEFAULT(SHARED)
!$OMP& PRIVATE(i,j,kx,ky,ii,jj,kz,ran,kp2,const1,const2,const3,indice)
!$OMP& PRIVATE(Axp,Azp,Ays,Azs,Ax,Ay,Az,Axr,Ayr,Azr,indicex,indicey)        
!$OMP DO SCHEDULE(DYNAMIC)  COLLAPSE(2)
!$OMP& REDUCTION(+:fluxinc,fluxref,fluxtra)         
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky

               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
                  if (i.ge.0) then
                     indicex=i+1
                  else
                     indicex=nfft2d+i+1
                  endif
                  if (j.ge.0) then
                     indicey=j+1
                  else
                     indicey=nfft2d+j+1
                  endif
                  indice=indicex+nfft2d*(indicey-1)
                  
                  kz=dsqrt(k0*k0-kp2)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))
                  const3=cdexp(-icomp*(kx*x0+ky*y0-kz*z0))
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp*const2
                  Ay=E0*dsin(psi)*Ays*const2
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const2                
c     changement de base pour retourner dans la base de la surface
                  Axr=E0*dcos(psi)*Axp*const3
                  Ayr=E0*dsin(psi)*Ays*const3
                  Azr=-E0*(dcos(psi)*Azp+dsin(psi)*Azs)*const3

                  if (nside.eq.1) then
                     Eloinx(indice)=Ax
                     Eloiny(indice)=Ay
                     Eloinz(indice)=Az
                  elseif (nside.eq.-1) then
                     Eloinx(indice)=Axr
                     Eloiny(indice)=Ayr
                     Eloinz(indice)=Azr
                  endif
                  
                  fluxinc=fluxinc+(dreal(Ax*dconjg(Ax)) +dreal(Ay
     $                 *dconjg(Ay))+dreal(Az*dconjg(Az)))*kz
                  fluxinc=fluxinc+(dreal(Axr*dconjg(Axr)) +dreal(Ayr
     $                 *dconjg(Ayr))+dreal(Azr*dconjg(Azr)))*kz      
                  fluxref=fluxref+(dreal((Ediffkzneg(ii,jj,1)+Axr)
     $                 *dconjg(Ediffkzneg(ii,jj,1)+Axr)) +
     $                 dreal((Ediffkzneg(ii,jj,2)+Ayr)
     $                 *dconjg(Ediffkzneg(ii,jj,2)+Ayr))
     $                 +dreal((Ediffkzneg(ii,jj,3)+Azr)
     $                 *dconjg(Ediffkzneg(ii,jj,3)+Azr)))*kz
                  fluxtra=fluxtra+(dreal((Ediffkzpos(ii,jj,1)+Ax)
     $                 *dconjg(Ediffkzpos(ii,jj,1)+Ax)) +
     $                 dreal((Ediffkzpos(ii,jj,2)+Ay)
     $                 *dconjg(Ediffkzpos(ii,jj,2)+Ay))
     $                 +dreal((Ediffkzpos(ii,jj,3)+Az)
     $                 *dconjg(Ediffkzpos(ii,jj,3)+Az)))*kz
               
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      endif


      
      tmp=4.d0*pi*pi*deltaky*deltakx/(k0*8.d0*pi*1.d-7*299792458.d0)
      write(*,*) 'Incident flux         :',fluxinc*tmp,'W'
      write(*,*) 'Reflected flux        :',fluxref*tmp,'W'
      write(*,*) 'Transmitted flux      :',fluxtra*tmp,'W'
      write(*,*) 'Total flux            :',fluxref*tmp+fluxtra*tmp,'W'
      efficacite=(fluxref+fluxtra)/fluxinc
      efficaciteref=fluxref/fluxinc
      efficacitetrans=fluxtra/fluxinc
      
      end
