      subroutine inverserigpg(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $     ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz,tabdip
     $     ,ntotalm,ntotal,ldabi,nlar,nmax,ndipole,nxm ,nym,nzm ,nx,ny
     $     ,nz ,nx2 ,ny2,nxy2,nz2,nbsphere,nbsphere3,XI,XR,wrk,FF ,FF0
     $     ,FFloc ,FFprecon ,polarisa ,methodeit,tol,tol1,nloop,ncompte
     $     ,nlim,k0 ,aretecube ,chanxxvect,chanxyvect,chanxzvect
     $     ,chanyyvect ,chanyzvect ,chanzzvect ,sdetnn, planf ,planb
     $     ,nstop,infostr)

      
      implicit none

      integer ntotalm,ntotal,nmax,nxm,nym,nzm,nx,ny,nz,nx2,ny2,nxy2,nz2
      integer ncompte,nt,ldabi, nlar,i,ii,jj,k,nbsphere,nbsphere3,nstop
      integer NLIM,ndim,nou,nstat,nloop,STEPERR,ndipole,is,nr,nu,np,ns
     $     ,nq,LL ,JL
      integer nprecon,ntest,nmaxcompo
      DOUBLE PRECISION  NORM,TOL,tol1,tole  , tmp,k0,aretecube,RESIDU
      double complex ALPHA,BETA,SIGMA,GPETA,DZETA,R0RN,QMR1,QMR2,QMR3
     $     ,QMR4,QMR5,QMR6,QMR7,QMR8,QMR9,DOTS1,DOTS2,DOTS3,DOTS4,OMEGA
     $     ,RHO,ctmp1,ctmp2
      integer, dimension(nxm*nym*nzm) :: Tabdip
      double complex , dimension (:), allocatable :: vectxx,vectyy
     $     ,vectzz
      double complex, dimension(3*nxm*nym*nzm) :: xr,xi
      double complex, dimension(3*nxm*nym*nzm,nlar) :: wrk
      double complex, dimension(3*nxm*nym*nzm) :: FF,FF0,FFloc,FFprecon
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx,
     $     FFTTENSORxy,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz, FFTTENSORzz
     $     ,vectx,vecty,vectz
      double complex chanxxvect(nxm*nym*nzm) ,chanxyvect(nxm*nym*nzm)
     $     ,chanxzvect(nxm*nym*nzm) ,chanyyvect(nxm*nym*nzm)
     $     ,chanyzvect(nxm*nym*nzm) ,chanzzvect(nxm*nym*nzm)
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa
      double complex sdetnn(3*nzm,3*nzm,nxm*nym)
      integer , dimension (:,:),allocatable ::ipvtnn

      character(12) methodeit
      character(64) infostr
      integer*8 planf,planb
      integer*8 plan2b,plan2f
      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD

      write(*,*) 'Left preconditionner used non optimized'
c     Initialise les tableaux pour preconditionner
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC) 
      do i=1,nbsphere3
         FFprecon(i)=FF0(i)
         wrk(i,1)=xi(i)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      nmaxcompo=nx*ny
      allocate(vectxx(1:nmaxcompo))
      allocate(vectyy(1:nmaxcompo))
      allocate(vectzz(1:nmaxcompo))
      allocate(ipvtnn(nz*3,nmaxcompo))

      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64

#ifdef USE_FFTW
      call dfftw_plan_dft_2d(plan2b,nx,ny, vectxx, vectxx ,FFTW_BACKWARD
     $     ,FFTW_ESTIMATE)
      call dfftw_plan_dft_2d(plan2f,nx,ny, vectxx, vectxx ,FFTW_FORWARD
     $     ,FFTW_ESTIMATE)
#endif

      ntest=0
      write(*,*) 'Create Chan s matrix'
      call preconditionneur(FFprecon,xi,xr,tabdip,polarisa,chanxxvect
     $     ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect,nx,ny
     $     ,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest,sdetnn
     $     ,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f,infostr ,nstop)

c     calcul P-1 FF0
      ntest=1
      call preconditionneur(FFprecon,xi,xr,tabdip,polarisa,chanxxvect
     $     ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect,nx,ny
     $     ,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest,sdetnn
     $     ,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f,infostr ,nstop)
      write(*,*) 'Compte the new incident field done'

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  
      do i=1,nbsphere3
         xi(i)=wrk(i,1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

      if (nstop.eq.1) return

      nou=0
      ndim=nbsphere3

      if (methodeit(1:7).eq.'GPBICG1') then
 2002    call GPBICG(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
c            write(*,*) 'xi',xi(i),i
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                   
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax,ndipole,nxm,nym,nzm,nx,ny,nz
     $        ,nx2,ny2 ,nxy2 ,nz2,XI,XR,planf,planb)

         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)

         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2002

c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1
        
         
      elseif (methodeit(1:7).eq.'GPBICG2') then
         write(*,*) 'totototo'
 2009    call GPBICG2(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)            
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL              
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                     
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)            
         do i=1,nbsphere
            k=3*(i-1)
            xr(k+1)=xr(k+1)/polarisa(i,1,1)
            xr(k+2)=xr(k+2)/polarisa(i,2,2)
            xr(k+3)=xr(k+3)/polarisa(i,3,3)
         enddo      
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2009
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            FFloc(i)=FF(i)/polarisa(1,1,1)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'GPBICG2 tol1',tol1,tmp/NORM
         
      elseif (methodeit(1:10).eq.'GPBICGsafe') then
 2019    call GPBICGsafe(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP
     $        ,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL             
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo  
!$OMP ENDDO 
!$OMP END PARALLEL  
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2019
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'GPBICGsafe tol1',tol1,tmp/NORM
      elseif (methodeit(1:10).eq.'GPBICGplus') then
 2016    call GPBICGplus(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP
     $        ,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL             
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         endif
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)  
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo  
!$OMP ENDDO 
!$OMP END PARALLEL  
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2016
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)  
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'GPBICGsafe tol1',tol1,tmp/NORM
         
      elseif (methodeit(1:8).eq.'GPBICGAR') then
 2010    call GPBICGAR(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim
     $        ,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL              
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo  
!$OMP ENDDO 
!$OMP END PARALLEL              
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2010
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'GPBICGAR1 tol1',tol1,tmp/NORM
         
      elseif (methodeit(1:9).eq.'GPBICGAR2') then
 2011    call GPBICGAR2(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP
     $        ,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN,NSTAT,STEPERR) 
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)               
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo 
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2011
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'GPBICGAR2 tol1',tol1,tmp/NORM
      elseif (methodeit(1:12).eq.'BICGSTARPLUS') then
 2015    call GPBICGSTARPLUS(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK
     $        ,NLOOP,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,R0RN ,NSTAT
     $        ,STEPERR)      
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                   
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2015
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1
c     write(*,*) 'GPBICG1 tol1',tol1,tmp/NORM
         
         
         
      elseif (methodeit(1:5).eq.'TFQMR') then
 2004    call TFQMR(FFloc,Xi,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,nloop
     $        ,NLIM,TOL,NORM,QMR1,QMR2,QMR3,QMR4,QMR5,QMR6,NSTAT
     $        ,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               xi(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL              
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz ,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI ,XR,planf,planb)

         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstat.ne.1) goto  2004
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM
         nloop=nloop+1
c     write(*,*) 'TFQMR tol1',tol1,NORM,xr(1),FF(1),FFprecon(1)
         
      elseif (methodeit(1:5).eq.'CG') then

 2005    call ZCG(XI,XR,FFprecon,NORM,WRK,QMR1,QMR2,QMR3,LDABI,NDIM,NLAR
     $        ,NOU,NSTAT,NLOOP,NLIM,TOLE,TOL)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)       
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)               
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)       
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz ,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI ,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstat.ne.1) goto  2005
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM

c     write(*,*) 'ZCG tol1',tol1
         
      elseif (methodeit(1:8).eq.'BICGSTAB') then

 2006    call PIMZBICGSTAB(FFLOC,Xi,XR,FFprecon,ldabi,nlar,ndim,nou,WRK
     $        ,QMR1,QMR2,QMR3,NORM,TOL,nloop,nlim,NSTAT,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               xi(i)=FFLOC(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
         endif
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo 
!$OMP ENDDO 
!$OMP END PARALLEL             

         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz ,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx ,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI ,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)

         if (nstat.ne.1) goto  2006

         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
         
c     write(*,*) 'PIMZBICGSTAB tol1',tol1
         
      elseif (methodeit(1:12).eq.'QMRBICGSTAB1') then
         nt=1
 2007    call QMRBICGSTAB(FFloc,Xi,XR,FFprecon,ldabi,ndim,nlar,nou,WRK
     $        ,nloop,nlim,TOL,TOLE,NORM,QMR1,QMR2,QMR3,QMR4,QMR5
     $        ,QMR6,QMR7,QMR8,QMR9,NT,NSTAT ,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1

         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i)            
!$OMP DO SCHEDULE(STATIC) 
            do i=1,nbsphere3
               xi(i)=FFLOC(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL              
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo  
!$OMP ENDDO 
!$OMP END PARALLEL            
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz ,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx ,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI ,XR,planf,planb)

         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)

         if (nstat.ne.1) goto  2007
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM

c     write(*,*) 'QMRBICGSTAB1 tol1',tol1
         
      elseif (methodeit(1:12).eq.'QMRBICGSTAB2') then

         nt=2
 2008    call QMRBICGSTAB(FFloc,Xi,XR,FFprecon,ldabi,ndim,nlar,nou,WRK
     $        ,nloop,nlim,TOL,TOLE,NORM,QMR1,QMR2,QMR3,QMR4,QMR5
     $        ,QMR6,QMR7,QMR8,QMR9,NT,NSTAT ,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               xi(i)=FFLOC(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                  
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k)    
!$OMP DO SCHEDULE(STATIC) 
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo 
!$OMP ENDDO 
!$OMP END PARALLEL             
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz ,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx ,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI ,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstat.ne.1) goto  2008
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM

c     write(*,*) 'QMRBICGSTAB2 tol1',tol1
      elseif (methodeit(1:7).eq.'GPBICOR') then
 2013    call GPBICOR(XI,XR,FFprecon,FFloc,ldabi,ndim,nlar,nou,WRK
     $        ,NLOOP,Nlim,TOL,NORM,ALPHA,BETA,GPETA,DZETA,QMR1
     $        ,QMR2,NSTAT,STEPERR)
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                   
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL  
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2013
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM

c     write(*,*) 'GPBICOR tol1',tol1

      elseif (methodeit(1:7).eq.'CORS') then
 2014    call CORS(XI,XR,FFprecon,ldabi,ndim,nlar,nou,WRK,NLOOP,Nlim,TOL
     $        ,NORM,ALPHA,GPETA,DZETA,BETA,NSTAT,STEPERR)
         
         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               FFloc(i)=xi(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                   
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny
     $        ,nz,nx2 ,ny2,nxy2 ,nz2,XI,XR,planf,planb)
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)

         if (nstat.ne.1) goto  2014
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL          
         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1
c         write(*,*) 'CORS tol1',tol1,tmp/NORM
         
       
 
      elseif (methodeit(1:4).eq.'IDRS') then
         if (methodeit(1:5).eq.'IDRS2') then
            ns=2
         elseif (methodeit(1:5).eq.'IDRS4') then
            ns=4
         elseif (methodeit(1:5).eq.'IDRS8') then
            ns=8
         endif
         
 2020    call IDRS(FFloc,Xi,XR,FFprecon,ldabi,ndim,nlar,nou,ns,WRK,NLOOP
     $        ,NLIM,TOL,RESIDU,NORM,ALPHA,is,NSTAT ,STEPERR)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               xi(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            

         
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax,ndipole,nxm,nym,nzm,nx,ny,nz
     $        ,nx2,ny2 ,nxy2 ,nz2,XI,XR,planf,planb)

         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2020
c     compute the Residue
         tol1=0.d0
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL

         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1
        
c         write(*,*) 'GPBICG1 tol1',tol1,tmp/NORM,ncompte
c         write(*,*) 'FF',FF
     	elseif (methodeit(1:9).eq.'BICGLSTAB') then      
         if (methodeit(1:10).eq.'BICGSTABL2') then
            ns=2
         elseif (methodeit(1:10).eq.'BICGSTABL4') then
            ns=4
         elseif (methodeit(1:10).eq.'BICGSTABL8') then
            ns=8
         endif 
 2021    call ZBICGSTABL(XI,XR,FFprecon,FFloc,NORM,WRK,ALPHA,RHO,OMEGA
     $        ,ns,nr,nu,JJ,LDABI,NDIM,NLAR,NOU,NSTAT,STEPERR,NLOOP,NLIM
     $        ,TOLE,TOL)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               xi(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            

         
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL

         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax,ndipole,nxm,nym,nzm,nx,ny,nz
     $        ,nx2,ny2 ,nxy2 ,nz2,XI,XR,planf,planb)
         
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)

         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2021
c     compute the Residue
         tol1=0.d0

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL

         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1
	elseif (methodeit(1:11).eq.'GPBICGSTABL') then      
         if (methodeit(1:12).eq.'GPBICGSTABL2') then
            LL=2
         elseif (methodeit(1:12).eq.'GPBICGSTABL4') then
            LL=4
         elseif (methodeit(1:12).eq.'GPBICGSTABL8') then
            LL=8
         endif
 2022    call GPBICGSTABL(XI,XR,FFprecon,FFloc,NORM,WRK,ALPHA,BETA,SIGMA
     $        ,RHO,LL,LDABI,NDIM,NLAR,NOU,nr,np,ns,nq,JL,NSTAT,STEPERR
     $        ,NLOOP ,NLIM ,TOLE,TOL)

         if (nstat.lt.0) then
            nstop=1
            infostr='Problem to solve Ax=b!'
            write(*,*) 'Problem to solve Ax=b',nstat,STEPERR
            return
         endif
         ncompte=ncompte+1
         if (nstat.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
            do i=1,nbsphere3
               xi(i)=FFloc(i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)                
         do i=1,nbsphere3
            xr(i)=xi(i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            k=3*(i-1)
            xi(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)*xr(k+2)
     $           +polarisa(i,1,3)*xr(k+3)
            xi(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)*xr(k+2)
     $           +polarisa(i,2,3)*xr(k+3)
            xi(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)*xr(k+2)
     $           +polarisa(i,3,3)*xr(k+3)
         enddo        
!$OMP ENDDO 
!$OMP END PARALLEL

         call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $        ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $        ,tabdip ,ntotalm,ntotal,nmax,ndipole,nxm,nym,nzm,nx,ny,nz
     $        ,nx2,ny2 ,nxy2 ,nz2,XI,XR,planf,planb)
         
         call preconditionneur(xr,FF,FFloc,tabdip,polarisa,chanxxvect
     $        ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect
     $        ,nx,ny,nz,nbsphere,ndipole,nxm ,nym,nzm,aretecube,k0,ntest
     $        ,sdetnn,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f
     $        ,infostr ,nstop)
         
         if (nstop == -1) then
            infostr = 'Calculation cancelled during iterative method'
            return
         endif

         if (nstat.ne.1) goto  2022
c     compute the Residue
         tol1=0.d0

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i) 
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:tol1)           
         do i=1,nbsphere3
            xr(i)=xr(i)-FFprecon(i)
            FF(i)=xi(i)
            tol1=tol1+dreal(xr(i)*dconjg(xr(i)))
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
         tol1=dsqrt(tol1)/NORM
c     as we begin with ITNO=-1
         nloop=nloop+1         
           
      else
         write(*,*) 'Iterative method not correct',methodeit
         nstop=1
         infostr='Iterative method not correct!'         
      endif

      deallocate(vectxx)
      deallocate(vectyy)
      deallocate(vectzz)
      deallocate(ipvtnn)
      end
