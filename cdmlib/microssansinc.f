      subroutine microsdfneg(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $     ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $     ,ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2,ny2
     $     ,nxy2,nz2,ndipole,nbsphere,nbsphere3,nproche,nrig,nfft2d
     $     ,tabfft2 ,tabdip,XI ,XR,wrk ,FF,FF0 ,FFloc ,polarisa ,epsilon
     $     ,methodeit,tolinit,tol1 ,nloop ,ncompte,nlim,xs ,ys,zs
     $     ,aretecube ,numaper,numaperinc,npolainc,nquicklens,eps0,k0,P0
     $     ,irra,w0 ,gross,zlens,Eimagex,Eimagey,Eimagez ,Efourierx
     $     ,Efouriery ,Efourierz,Ediffkzpos,Ediffkzneg,kxy,xy ,nside
     $     ,planf ,planb ,plan2f ,plan2b,nmat,file_id ,group_idmic,nstop
     $     ,infostr)

#ifdef USE_HDF5
      use HDF5
#endif

      implicit none
c     variables en argument      
      integer ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2
     $     ,ny2,nxy2,nz2,nbsphere,nbsphere3,nloop,ncompte,nlim,nstop
     $     ,nfft2d ,ndipole,nproche,nrig,nmat,ipol,npol,npolainc
     $     ,nquicklens ,nside,niter,niterii
      integer tabfft2(nfft2d)
      double precision tol,tolinit,tol1,aretecube,eps0,k0,P0,irra,w0,I0
     $     ,numaper,numaperinc,gross,deltax,x,y,z,zlens
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys,zs
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx,
     $     FFTTENSORxy,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz, FFTTENSORzz
     $     ,vectx,vecty,vectz
      double complex, dimension(3*nxm*nym*nzm) :: xr,xi
      double complex, dimension(3*nxm*nym*nzm,nlar) :: wrk
      double complex, dimension(3*nxm*nym*nzm) :: FF,FF0,FFloc
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa,epsilon
      double complex E0,Eloc(3),Em(3),epsani(3,3),icomp
      double complex Eimagex(nfft2d*nfft2d),Eimagey(nfft2d*nfft2d)
     $     ,Eimagez(nfft2d*nfft2d),Efourierx(nfft2d*nfft2d)
     $     ,Efouriery(nfft2d*nfft2d) ,Efourierz(nfft2d*nfft2d)
      double complex Ediffkzpos(nfft2d,nfft2d,3),Ediffkzneg(nfft2d
     $     ,nfft2d,3),zfocus
      double precision kxy(nfft2d),xy(nfft2d)
      integer, dimension(nxm*nym*nzm) :: Tabdip
      character(64) infostr,beam
      character(12) methodeit
      
      integer*8 planf,planb,plan2f,plan2b,planfn,planbn
      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD

c     variable pour le bright field
      integer i,j,ii,jj,k,kk,ideltam,idelta,nsens,imaxk0,indice
     $     ,indicex,indicey,nfft2d2,ikxinc,jkyinc,imul,imaxinc
      double precision deltak,numaperk,phi,theta,zero,pi,kx,ky,kz,ss,pp
     $     ,u1,u2,tmp,normal(3),xmin,xmax,ymin,ymax,deltakx,deltaky
     $     ,kxinc,kyinc,deltatheta,u(3),v(3),sintmp,costmp,deltakm
      double complex tmpx,tmpy,tmpz,Ex,Ey,Ez,Emx,Emy,Emz,ctmp,ctmp1

      character(LEN=100) :: datasetname

#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif

      integer(hid_t) :: file_id
      integer(hid_t) :: group_idmic
      integer :: dim(4)
      integer error
      write(*,*) '***** Dark field and phase microscope *****'
      write(*,*) 'nside',nside
c     initialise
      zero=0.d0
      pi=dacos(-1.d0)
      beam='pwavelinear'
      nfft2d2=nfft2d/2
      npolainc=0
      icomp=(0.d0,1.d0)
      numaperk=k0*numaperinc
      x=0.d0
      y=0.d0
      z=0.d0
c     
      if (numaperinc.ge.1.d0.or.numaper.le.0.d0) then
         infostr='NA inc strictly between 0 and 1'
         nstop=1
         return
      endif

c     calcul de deltak      
      xmax=-1.d300
      xmin=1.d300
      ymax=-1.d300
      ymin=1.d300
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) REDUCTION(max:xmax,ymax)
!$OMP& REDUCTION(min:xmin,ymin)      
      do i=1,nbsphere
         xmax=max(xmax,xs(i))
         xmin=min(xmin,xs(i))
         ymax=max(ymax,ys(i))
         ymin=min(ymin,ys(i))     
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      deltakm=pi/max(xmax-xmin,ymax-ymin)
      deltax=aretecube
      
      if (nquicklens.eq.1) then
         
       deltakx=2.d0*pi/(aretecube*dble(nfft2d))

         if (deltakx.ge.numaperk) then
            nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 1'
            return
         endif        
         imul=idint(deltakm/deltakx)
         if (imul.eq.0) then
            nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 2'
            return            
         endif
 223     deltak=deltakx*dble(imul)
         write(*,*) 'change delta k incident:',deltak,'m-1',imul
         imaxinc=nint(k0/deltak)+1
         if (imaxinc.le.3) then
            imul=imul-1
            if (imul.eq.0) then
               nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 3'
               return
            endif
            goto 223
         endif
         write(*,*) 'Step size delta k diffracted: ',deltakx,'m-1'

      else
         k=0
 224     deltakx=2.d0*pi/(dble(nfft2d)*aretecube)/dble(2**k)
         imaxk0=nint(k0/deltakx)+1            
         if (imaxk0.le.5) then
            k=k+1
            write(*,*) 'change delta k diffracted:',deltakx,'m-1',k
            goto 224
         endif
         write(*,*) 'Final delta k diffracted',deltakx,'m-1'

         k=0
 222     deltak=deltakx*dnint(deltakm/deltakx)/dble(2**k)
         imaxinc=nint(k0/deltak)+1
         if (imaxinc.le.2) then
            k=k+1
            write(*,*) 'change delta k incident:',deltak,'m-1',k
            goto 222
         endif
         write(*,*) 'Final delta k incident : ',deltak,'m-1'
      endif

      deltax=aretecube
      deltatheta=deltak/k0*numaperinc
      ideltam=max(int(2.d0*pi/deltatheta)+1,8)
   

c     initalise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
      do i=1,nfft2d*nfft2d
         Eimagex(i)=0.d0
         Eimagey(i)=0.d0
         Eimagez(i)=0.d0                          
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

      npolainc=0
      npol=1
      niter=ideltam*npol
      if (npolainc.eq.0) then
         npol=2
         niter=ideltam*npol
      endif

c     calcul puissance
      P0=P0/dble(npol*ideltam)
      call irradiance(P0,w0,E0,irra)
      I0=cdabs(E0)**2
      write(*,*) 'E0',E0,I0,P0,npol,ideltam
      niterii=0
      do ipol=1,npol
    
         if (npolainc.eq.1) then
            ss=1.d0
            pp=0.d0
         elseif (npolainc.eq.2) then
            ss=0.d0
            pp=1.d0
         else
            if (ipol.eq.1) then
               ss=1.d0
               pp=0.d0
            else
               ss=0.d0
               pp=1.d0
            endif
         endif

c     sommation 
         do idelta=0,ideltam-1
c         do idelta=0,1
            niterii=niterii+1
            write(*,*) '*** incidence',niterii,'/',niter,' *****'
            phi=dble(idelta)*2.d0*pi/dble(ideltam)
            theta=dasin(numaperinc)
            kxinc=dcos(phi)*k0*numaperinc
            kyinc=dsin(phi)*k0*numaperinc

c     calcul champ incident
            
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
            do i=1,nbsphere           
               call ondeplanekxky(xs(i),ys(i),zs(i),k0,E0,ss,pp,kxinc
     $              ,kyinc,FF0(3*i-2),FF0(3*i-1),FF0(3*i),nstop
     $              ,infostr)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
            if (nstop.eq.1) return
            
c     calcul champ local
            if (nrig.eq.0) then
               tol=tolinit
               ncompte=0
               nloop=0
               
               if (nproche.eq.-1) then
                  call inverserig(FFTTENSORxx,FFTTENSORxy ,FFTTENSORxz
     $                 ,FFTTENSORyy,FFTTENSORyz ,FFTTENSORzz,vectx,vecty
     $                 ,vectz ,Tabdip ,ntotalm,ntotal,ldabi,nlar,nmax
     $                 ,ndipole,nxm ,nym ,nzm,nx,ny,nz,nx2,ny2,nxy2,nz2
     $                 ,nbsphere,nbsphere3,XI ,XR,wrk,FF,FF0,FFloc
     $                 ,polarisa,methodeit,tol,tol1,nloop,ncompte,nlim
     $                 ,planf,planb,nstop,infostr)
                  if (nstop.eq.1) return
               else

                  call inverserigopt(FFTTENSORxx,FFTTENSORxy
     $                 ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz ,FFTTENSORzz
     $                 ,vectx,vecty,vectz,ntotalm ,ntotal ,ldabi,nlar
     $                 ,nmax,nxm,nym,nzm,nx,ny ,nz,nx2,ny2 ,nxy2,nz2
     $                 ,nbsphere,nbsphere3,XI ,XR,wrk,FF,FF0 ,FFloc
     $                 ,polarisa,methodeit ,tol,tol1,nloop ,ncompte,nlim
     $                 ,planf,planb,nstop ,infostr)
                  if (nstop.eq.1) return
               endif
            elseif (nrig.eq.1) then 
c     **************************************        
c     Renormalized Born approximation field
c     **************************************
               write(*,*) 'Renormalized Born approximation'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere3
                  FFloc(i)=FF0(i)
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
                  k=3*(i-1)
                  do ii=1,3
                     FF(k+ii)=0.d0
                     do jj=1,3
                        FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                       *FFloc(k+jj)
                     enddo
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
            elseif (nrig.eq.2) then 
c     **************************************        
c     Born approximation field
c     **************************************
               write(*,*) 'Born approximation'
               nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,kk,Em,Eloc,ii,jj,epsani)   
!$OMP DO SCHEDULE(STATIC) 
               do k=1,nbsphere
                  kk=3*(k-1)
                  Em(1)=FF0(kk+1)
                  Em(2)=FF0(kk+2)
                  Em(3)=FF0(kk+3)
                  do ii=1,3
                     do jj=1,3
                        epsani(ii,jj)=epsilon(k,ii,jj)
                     enddo
                  enddo 
                  call local_macro(Eloc,Em,epsani,aretecube,k0
     $                 ,nsens)
                  FFloc(kk+1)=Eloc(1)
                  FFloc(kk+2)=Eloc(2)
                  FFloc(kk+3)=Eloc(3)
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

c     dipole
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
                  k=3*(i-1)
                  do ii=1,3
                     FF(k+ii)=0.d0
                     do jj=1,3
                        FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                       *FFloc(k+jj)
                     enddo
                  enddo
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
            elseif (nrig.eq.3) then
c     renormalized Born ordre 1
               write(*,*) 'Born series order 1'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC) 
               do i=1,nbsphere3
                  xr(i)=FF0(i)           
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)  
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
                  k=3*(i-1)
                  xi(k+1)=-polarisa(i,1,1)*xr(k+1)-polarisa(i,1,2)
     $                 *xr(k+2)-polarisa(i,1,3)*xr(k+3)
                  xi(k+2)=-polarisa(i,2,1)*xr(k+1)-polarisa(i,2,2)
     $                 *xr(k+2)-polarisa(i,2,3)*xr(k+3)
                  xi(k+3)=-polarisa(i,3,1)*xr(k+1)-polarisa(i,3,2)
     $                 *xr(k+2)-polarisa(i,3,3)*xr(k+3)
               enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
               
               call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $              ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $              ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $              ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $              ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)       
               do i=1,nbsphere
                  k=3*(i-1)
                  FFloc(k+1)=xr(k+1)
                  FFloc(k+2)=xr(k+2)
                  FFloc(k+3)=xr(k+3)
                  FF(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)
     $                 *xr(k+2)+polarisa(i,1,3)*xr(k+3)
                  FF(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)
     $                 *xr(k+2)+polarisa(i,2,3)*xr(k+3)
                  FF(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)
     $                 *xr(k+2)+polarisa(i,3,3)*xr(k+3)
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

               
            elseif (nrig.eq.4) then
c     Rytov renormalize
               write(*,*) 'Rytov approximation on local field'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere3
                  xr(i)=0.d0       
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
                  k=3*(i-1)
                  xi(k+1)=-polarisa(i,1,1)*FF0(k+1)-polarisa(i,1
     $                 ,2)*FF0(k+2)-polarisa(i,1,3)*FF0(k+3)
                  xi(k+2)=-polarisa(i,2,1)*FF0(k+1)-polarisa(i,2
     $                 ,2)*FF0(k+2)-polarisa(i,2,3)*FF0(k+3)
                  xi(k+3)=-polarisa(i,3,1)*FF0(k+1)-polarisa(i,3
     $                 ,2)*FF0(k+2)-polarisa(i,3,3)*FF0(k+3)
               enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
               
               call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $              ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $              ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $              ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $              ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
               do i=1,nbsphere
                  k=3*(i-1)
                  tmp=dsqrt(dreal(FF0(k+1)*dconjg(FF0(k+1))+FF0(k
     $                 +2)*dconjg(FF0(k+2))+FF0(k+3)*dconjg(FF0(k
     $                 +3))))*1.d-6
                  if (cdabs(FF0(k+1)).le.tmp) then
                     FFloc(k+1)=0.d0
                  else
                     FFloc(k+1)=FF0(k+1)*cdexp(xr(k+1)/FF0(k+1))
                  endif
                  if (cdabs(FF0(k+2)).le.tmp) then
                     FFloc(k+2)=0.d0
                  else
                     FFloc(k+2)=FF0(k+2)*cdexp(xr(k+2)/FF0(k+2))
                  endif
                  if (cdabs(FF0(k+3)).le.tmp) then
                     FFloc(k+3)=0.d0
                  else
                     FFloc(k+3)=FF0(k+3)*cdexp(xr(k+3)/FF0(k+3))
                  endif
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
               do i=1,nbsphere
                  k=3*(i-1)  
                  FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                 ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                  FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                 ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                  FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                 ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

            elseif (nrig.eq.5) then
c     Rytov
               write(*,*)
     $              'Rytov approximation on macroscopic field'
c     a3=aretecube*aretecube*aretecube
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)   
!$OMP DO SCHEDULE(STATIC) 
               do i=1,nbsphere
                  k=3*(i-1)
                  xr(k+1)=-FF0(k+1)*(epsilon(i,1,1)-eps0)/3.d0
                  xr(k+2)=-FF0(k+2)*(epsilon(i,2,2)-eps0)/3.d0
                  xr(k+3)=-FF0(k+3)*(epsilon(i,3,3)-eps0)/3.d0
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,Em,Eloc,ii,jj,epsani)
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
                  k=3*(i-1)
                  Em(1)=FF0(k+1)
                  Em(2)=FF0(k+2)
                  Em(3)=FF0(k+3)
                  do ii=1,3
                     do jj=1,3
                        epsani(ii,jj)=epsilon(i,ii,jj)
                     enddo
                  enddo 
                  call local_macro(Eloc,Em,epsani,aretecube,k0
     $                 ,nsens)
                  FF(k+1)=Eloc(1)
                  FF(k+2)=Eloc(2)
                  FF(k+3)=Eloc(3)
                  xi(k+1)=-polarisa(i,1,1)*FF(k+1)-polarisa(i,1,2)
     $                 *FF(k+2)-polarisa(i,1,3)*FF(k+3)
                  xi(k+2)=-polarisa(i,2,1)*FF(k+1)-polarisa(i,2,2)
     $                 *FF(k+2)-polarisa(i,2,3)*FF(k+3)
                  xi(k+3)=-polarisa(i,3,1)*FF(k+1)-polarisa(i,3,2)
     $                 *FF(k+2)-polarisa(i,3,3)*FF(k+3)
               enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
               
               call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $              ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $              ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $              ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $              ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
               do i=1,nbsphere
                  k=3*(i-1)
                  tmp=dsqrt(dreal(FF(k+1)*dconjg(FF(k+1))+FF(k+2)
     $                 *dconjg(FF(k+2))+FF(k+3)*dconjg(FF(k+3))))
     $                 *1.d -6
                  if (cdabs(FF(k+1)).le.tmp) then
                     FFloc(k+1)=0.d0
                  else
                     FFloc(k+1)=FF(k+1)*cdexp(xr(k+1)/FF(k+1))
                  endif
                  if (cdabs(FF(k+2)).le.tmp) then
                     FFloc(k+2)=0.d0
                  else
                     FFloc(k+2)=FF(k+2)*cdexp(xr(k+2)/FF(k+2))
                  endif
                  if (cdabs(FF(k+3)).le.tmp) then
                     FFloc(k+3)=0.d0
                  else
                     FFloc(k+3)=FF(k+3)*cdexp(xr(k+3)/FF(k+3))
                  endif
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)      
               do i=1,nbsphere
                  k=3*(i-1)  
                  FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                 ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                  FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                 ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                  FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                 ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      
               
            elseif (nrig.eq.6) then
c     Beam propagation method
               write(*,*) 'Beam propagation method'

               call beampropagationmacro(xs,ys,zs,aretecube,k0,w0 ,E0,ss
     $              ,pp,theta,phi,zero,zero,zero,beam ,epsilon,ndipole
     $              ,nx ,ny,nz,nxm,nym ,nzm,nmax ,nfft2d,Efourierx
     $              ,Efouriery,Efourierz,FF0,FFloc,FF ,plan2f,plan2b
     $              ,nstop,infostr)

               if (nstop.eq.1) return

               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
               do i=1,nbsphere
                  k=3*(i-1)  
                  FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                 ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                  FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                 ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                  FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                 ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL    
            elseif (nrig.eq.7) then
c     Beam propagation method
               call beampropagation(xs,ys,zs,aretecube,k0,w0,E0,ss ,pp
     $              ,theta,phi,zero,zero,zero,beam,epsilon ,ndipole,nx
     $              ,ny,nz,nxm,nym ,nzm,nmax,nfft2d ,Efourierx,Efouriery
     $              ,Efourierz,FF0 ,FFloc,FF,plan2f ,plan2b,nstop
     $              ,infostr)

               if (nstop.eq.1) return

               
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
               do i=1,nbsphere
                  k=3*(i-1)  
                  FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                 ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                  FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                 ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                  FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                 ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
               enddo            
!$OMP ENDDO 
!$OMP END PARALLEL    
            endif
c     *********************************************************
c     fin calcul champ local
c     *********************************************************
c     *********************************************************            
c     calcul champ diffracte
c     *********************************************************            
            if (nquicklens.eq.1) then
               call diffractefft2dlens(nx,ny,nz,nxm,nym,nzm,nfft2d
     $              ,tabfft2,k0,xs,ys,zs,aretecube,Efourierx,Efouriery
     $              ,Efourierz,FF,imaxk0,deltakx,deltaky,Ediffkzpos
     $              ,numaper,nside,plan2f,plan2b ,nstop ,infostr)
               if (nstop.eq.1) return
                  
            else
               
               
               deltaky=deltakx
               do i=-imaxk0,imaxk0
                  kx=deltakx*dble(i)
                  do j=-imaxk0,imaxk0
                     ky=deltaky*dble(j)
                     if (dsqrt(kx*kx+ky*ky).le.numaper) then
                        kz=dsqrt(k0*k0-kx*kx-ky*ky)
                        normal(1)=kx/k0
                        normal(2)=ky/k0
                        normal(3)=-dsqrt(1.d0-normal(1)*normal(1)
     $                       -normal(2)*normal(2))
                        Emx=0.d0
                        Emy=0.d0
                        Emz=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,kk,ctmp,ctmp1)
!$OMP DO SCHEDULE(STATIC) REDUCTION(+:Emx,Emy,Emz)
                        do ii=1,nbsphere
                           kk=3*(ii-1)
                           ctmp=cdexp(-icomp*k0*(normal(1)*xs(ii)
     $                          +normal(2)*ys(ii)+normal(3)
     $                          *zs(ii)))
                           ctmp1=normal(1)*FF(kk+1)+normal(2)
     $                          *FF(kk+2)+normal(3)*FF(kk+3)
                           Emx=Emx+ctmp*(FF(kk+1)-ctmp1*normal(1))
                           Emy=Emy+ctmp*(FF(kk+2)-ctmp1*normal(2))
                           Emz=Emz+ctmp*(FF(kk+3)-ctmp1*normal(3))
                        enddo
!$OMP ENDDO 
!$OMP END PARALLEL                     
                        ctmp=-2.d0*pi*icomp*kz                   
                        kk=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                        ii=imaxk0+i+1
                        jj=imaxk0+j+1
                        Ediffkzpos(ii,jj,1)=Emx*k0*k0/ctmp
                        Ediffkzpos(ii,jj,2)=Emy*k0*k0/ctmp
                        Ediffkzpos(ii,jj,3)=Emz*k0*k0/ctmp
                     endif
                  enddo
               enddo
            endif   
            
c     *********************************************************            
c     fin champ diffracte
c     *********************************************************
c     *********************************************************            
c     calcul image
c     *********************************************************  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
            do i=1,nfft2d*nfft2d
               Efourierx(i)=0.d0
               Efouriery(i)=0.d0
               Efourierz(i)=0.d0                          
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

            call deltakroutine(kxinc,kyinc,deltakx,deltaky,k0,ikxinc
     $           ,jkyinc)
c            write(*,*) 'ff ij',kxinc,kyinc,deltakx,deltaky,k0,ikxinc
c     $           ,jkyinc
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indicex,indicey,indice)
!$OMP& PRIVATE(kx,ky,kz,u1,u2,normal,tmp,tmpx,tmpy,tmpz,ii,jj)
!$OMP& PRIVATE(zfocus,Ex,Ey,Ez,u,v,costmp,sintmp)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do i=-imaxk0,imaxk0 
               do j=-imaxk0,imaxk0
                  if (i.ge.0) then
                     indicex=i+1
                  else
                     indicex=nfft2d+i+1
                  endif
                  if (j.ge.0) then
                     indicey=j+1
                  else
                     indicey=nfft2d+j+1
                  endif
                  indice=indicex+nfft2d*(indicey-1)

                  kx=deltakx*dble(i)
                  ky=deltaky*dble(j)
                  if (kx*kx+ky*ky.le.numaper*numaper) then
                     
                     kz=k0*k0-kx*kx-ky*ky
                     kz=dsqrt(kz)
                     zfocus=cdexp(icomp*kz*zlens)
                     u1=-ky/k0
                     u2=kx/k0
                     tmp=dsqrt(u1*u1+u2*u2)

                     ii=imaxk0+i+1
                     jj=imaxk0+j+1
                     
                     Efourierx(indice)=Ediffkzpos(ii,jj,1)*zfocus
                     Efouriery(indice)=Ediffkzpos(ii,jj,2)*zfocus
                     Efourierz(indice)=Ediffkzpos(ii,jj,3)*zfocus

                     
                     if (tmp.ne.0.d0) then
                    
                        u(1)=kx/k0
                        u(2)=ky/k0
                        u(3)=dsqrt(1.d0-u(1)*u(1)-u(2)*u(2))*dble(nside)
                        v(1)=-kx/k0/gross
                        v(2)=-ky/k0/gross
                        v(3)=dsqrt(1.d0-v(1)*v(1)-v(2)*v(2))*dble(nside)
                        u1=u(2)*v(3)-u(3)*v(2)
                        u2=-u(1)*v(3)+u(3)*v(1)
                        costmp=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
                        sintmp=dsqrt(u1*u1+u2*u2)    
                        u1=u1/sintmp
                        u2=u2/sintmp
                        tmp=dsqrt(u(3)/v(3))
                        tmpx=(u1*u1+(1.d0-u1*u1)*costmp)
     $                       *Efourierx(indice)+u1*u2*(1.d0-costmp)
     $                       *Efouriery(indice)+u2*sintmp
     $                       *Efourierz(indice)
                        tmpy=u1*u2*(1.d0-costmp)*Efourierx(indice)+(u2
     $                       *u2+(1.d0-u2 *u2)*costmp)*Efouriery(indice)
     $                       -u1*sintmp*Efourierz(indice)
                        tmpz=-u2*sintmp*Efourierx(indice)+u1*sintmp
     $                       *Efouriery(indice)+costmp*Efourierz(indice)
                        Efourierx(indice)=tmpx*tmp
                        Efouriery(indice)=tmpy*tmp
                        Efourierz(indice)=tmpz*tmp
                     endif
                  endif
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

            call fouriertoimage2(deltakx,deltaky,gross,Efourierx
     $           ,Efouriery,Efourierz,nfft2D,nfft2d2,plan2b,plan2f)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(indice)   
!$OMP DO SCHEDULE(STATIC)    
            do indice=1,nfft2d*nfft2d
               Eimagex(indice)=Efourierx(indice)
     $              *dconjg(Efourierx(indice))+Eimagex(indice)
               Eimagey(indice)=Efouriery(indice)
     $              *dconjg(Efouriery(indice))+Eimagey(indice)
               Eimagez(indice)=Efourierz(indice)
     $              *dconjg(Efourierz(indice))+Eimagez(indice)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL


            if (nstop == -1) then
               infostr ='Calculation cancelled during iterative method'
               return
            endif
            
         enddo
      enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(indice)   
!$OMP DO SCHEDULE(STATIC)        
      do indice=1,nfft2d*nfft2d
         Eimagex(indice)=cdsqrt(Eimagex(indice))
         Eimagey(indice)=cdsqrt(Eimagey(indice))
         Eimagez(indice)=cdsqrt(Eimagez(indice))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      deltax=2.d0*pi/dble(nfft2d)/deltakx
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
      do i=-nfft2d2,nfft2d2-1
         xy(i+nfft2d2+1)=deltax*dble(i)*gross
         kxy(i+nfft2d2+1)=deltakx*dble(i)/k0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL


      if (nmat.eq.0) then
         open(400,file='kxfourier.mat')
         open(401,file='ximage.mat')
         do i=-nfft2d2,nfft2d2-1
            write(401,*) xy(i+nfft2d2+1)
         enddo
         do i=-imaxk0,imaxk0
            kx=deltakx*dble(i)
            write(400,*) kx/k0
         enddo
         close(400)
         close(401)

         open(410,file='kxincidentdf.mat')
         open(411,file='kyincidentdf.mat')
         do idelta=0,ideltam-1
            phi=dble(idelta)*2.d0*pi/dble(ideltam)
            theta=dasin(numaperinc)
            kxinc=dcos(phi)*numaperinc
            kyinc=dsin(phi)*numaperinc
            write(410,*) kxinc
            write(411,*) kyinc
         enddo
         close(410)
         close(411)
         
         open(301,file='imagedf.mat')
         open(302,file='imagedfx.mat')
         open(303,file='imagedfy.mat')
         open(304,file='imagedfz.mat')


         do i=1,nfft2D*nfft2D
            write(301,*) dsqrt(dreal(Eimagex(i)**2.d0+Eimagey(i)**2.d0
     $           +Eimagez(i)**2.d0))
            write(302,*) dreal(Eimagex(i))
            write(303,*) dreal(Eimagey(i))
            write(304,*) dreal(Eimagez(i))
         enddo
         close(301)
         close(302)
         close(303)
         close(304)
      elseif (nmat.eq.2) then
         dim(1)=nfft2d
         dim(2)=nfft2d
         datasetname='x Image'
         call hdf5write1d(group_idmic,datasetname,xy,dim)
         k=0
         datasetname='Image dark field kz<0'
         call writehdf5mic(Eimagex,Eimagey,Eimagez,nfft2d ,imaxk0
     $        ,Ediffkzpos,k,datasetname,group_idmic)
         do idelta=0,ideltam-1
            phi=dble(idelta)*2.d0*pi/dble(ideltam)
            theta=dasin(numaperinc)
            kxy(idelta+1)=dcos(phi)*numaperinc
         enddo
         dim(1)=ideltam
         dim(2)=nfft2d
         datasetname='kx incident df'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         do idelta=0,ideltam-1
            phi=dble(idelta)*2.d0*pi/dble(ideltam)
            theta=dasin(numaperinc)
            kxy(idelta+1)=dsin(phi)*numaperinc
         enddo
         dim(1)=ideltam
         dim(2)=nfft2d
         datasetname='ky incident df'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
      endif
      
      end

c********************************************************
c********************************************************
c********************************************************            
      subroutine microsbfneg(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $     ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $     ,ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2,ny2
     $     ,nxy2,nz2,ndipole,nbsphere,nbsphere3,nproche,nrig,nfft2d
     $     ,tabfft2,tabdip,XI ,XR,wrk ,FF,FF0 ,FFloc ,polarisa ,epsilon
     $     ,methodeit,tolinit,tol1 ,nloop ,ncompte,nlim,xs ,ys,zs
     $     ,aretecube ,numaper,numaperinc,numaperinc2,npolainc
     $     ,nquicklens,eps0,k0,P0 ,irra,w0 ,gross,zlens,Eimagex ,Eimagey
     $     ,Eimagez ,Efourierx ,Efouriery ,Efourierz,Ediffkzpos
     $     ,Ediffkzneg,kxy,xy ,nside ,masque ,nmasque,ideltam, imaxk0
     $     ,nbitermic ,deltak,deltakx, ntypemic, planf ,planb ,plan2f
     $     ,plan2b,nmat ,file_id ,group_idmic,nstop ,infostr)
#ifdef USE_HDF5
      use HDF5
#endif

      implicit none
c     variables en argument      
      integer ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2
     $     ,ny2,nxy2,nz2,nbsphere,nbsphere3,nloop,ncompte,nlim,nstop
     $     ,nfft2d ,ndipole,nproche,nrig,nmat,ipol,npol,npolainc
     $     ,nquicklens ,nside,niter,niterii,ntypemic
      integer tabfft2(nfft2d)
      double precision tol,tolinit,tol1,aretecube,eps0,k0,P0,irra,w0,I0
     $     ,numaper,numaperinc,numaperinc2,gross,deltax,zlens
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys,zs
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx,
     $     FFTTENSORxy,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz, FFTTENSORzz
     $     ,vectx,vecty,vectz
      double complex, dimension(3*nxm*nym*nzm) :: xr,xi
      double complex, dimension(3*nxm*nym*nzm,nlar) :: wrk
      double complex, dimension(3*nxm*nym*nzm) :: FF,FF0,FFloc
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa,epsilon
      double complex E0,Eloc(3),Em(3),epsani(3,3)
      double complex Eimagex(nfft2d*nfft2d),Eimagey(nfft2d*nfft2d)
     $     ,Eimagez(nfft2d*nfft2d),Efourierx(nfft2d*nfft2d)
     $     ,Efouriery(nfft2d*nfft2d) ,Efourierz(nfft2d*nfft2d)
      double complex Ediffkzpos(nfft2d,nfft2d,3),Ediffkzneg(nfft2d
     $     ,nfft2d,3)
      double precision kxy(nfft2d),xy(nfft2d)
      integer, dimension(nxm*nym*nzm) :: Tabdip
      character(64) infostr,beam
      character(12) methodeit
      
      integer*8 planf,planb,plan2f,plan2b,planfn,planbn
      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD

c     variable pour le bright field
      integer i,j,ii,jj,k,kk,idelta,jdelta,nsens,imaxk0,indice ,indicex
     $     ,indicey,nfft2d2,ikxinc,jkyinc,imul,imaxinc
      double precision deltak,numaperk,phi,theta,zero,pi,kx,ky,kz,ss,pp
     $     ,u1,u2,tmp,normal(3),xmin,xmax,ymin,ymax,deltakx,deltaky
     $     ,kxinc,kyinc,u(3),v(3),sintmp,costmp,deltakm,x,y,z
      double complex tmpx,tmpy,tmpz,Ex,Ey,Ez,Emx,Emy,Emz,ctmp,ctmp1
     $     ,icomp,zfocus
      integer ideltam,nbitermic,nmasque
      double complex masque(nmasque*nmasque)
      character(LEN=100) :: datasetname,h5filemicros
      character(LEN=40) :: name
#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif

      integer(hid_t) :: file_id,file_idmicros
      integer(hid_t) :: group_idmic,group_id_conf,group_id_conf_fourier
     $     ,group_id_conf_image
      integer :: dim(4)
      integer error,nmicsavefield
      character( 4 )  fil4

      if (ntypemic.eq.1) then
         write(*,*) '***** Bright field microscope ******'
         write(*,*) 'Numerical aperture condenser: ',numaperinc
      elseif (ntypemic.eq.2) then
         write(*,*) '***** Dark field microscope ******'
         write(*,*) 'Numerical aperture condenser: ',numaperinc
         write(*,*) 'Central aperture            : ',numaperinc2
      elseif (ntypemic.eq.3) then
         write(*,*) '***** Schieren microscope ******'
         write(*,*) 'Numerical aperture condenser: ',numaperinc  
      elseif (ntypemic.eq.5) then
         write(*,*) '***** Experimental phase microscope ******'
         write(*,*) 'Numerical aperture condenser: ',numaperinc
         write(*,*) 'Central aperture            : ',numaperinc2
      endif

c     initialise
      zero=0.d0
      pi=dacos(-1.d0)
      beam='pwavelinear'
      nfft2d2=nfft2d/2
      npolainc=0
      icomp=(0.d0,1.d0)
      numaperk=k0*numaperinc

      if (nfft2d.gt.16384) then
         nstop=1
         infostr='nfft2d too large'
         return
      endif
      if (deltak.ge.numaperk) then
         nstop=1
         infostr='In FFT lens nfft2d too small'
         return
      endif

c     initalise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
      do i=1,nfft2d*nfft2d
         Eimagex(i)=0.d0
         Eimagey(i)=0.d0
         Eimagez(i)=0.d0                          
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
      
      npol=1
      niter=nbitermic
      if (npolainc.eq.0) then
         npol=2
         niter=2*nbitermic
      endif
c     calcul puissance
      P0=P0/dble(npol*niter)
      call irradiance(P0,w0,E0,irra)
      I0=cdabs(E0)**2

c     save if HDF5 for each angle of incidence.
      nmicsavefield=0
      if (nmicsavefield.ge.1) then
#ifdef USE_HDF5          
         h5filemicros='microscopy.h5'
         call hdf5create(h5filemicros, file_idmicros)
         if (ntypemic.eq.1) then
            call h5gcreate_f(file_idmicros,"Bright field", group_id_conf
     $           ,error)
            datasetname='numaper'
            call hdf5write(group_id_conf,datasetname,numaperinc,dim)
         elseif (ntypemic.eq.2) then
            call h5gcreate_f(file_idmicros,"Dark field", group_id_conf
     $           ,error)     
            datasetname='numaper'
            call hdf5write(group_id_conf,datasetname,numaperinc,dim)
            datasetname='numaper int'
            call hdf5write(group_id_conf,datasetname,numaperinc2,dim)
         elseif (ntypemic.eq.3) then
            call h5gcreate_f(file_idmicros,"Schieren", group_id_conf
     $           ,error)
            datasetname='numaper'
            call hdf5write(group_id_conf,datasetname,numaperinc,dim)
c            datasetname='kc x'
c            call hdf5write(group_id_conf,datasetname,kcnax,dim)
c            datasetname='kc y'
c            call hdf5write(group_id_conf,datasetname,kcnay,dim)
         elseif (ntypemic.eq.5) then
            call h5gcreate_f(file_idmicros,"Experimental phase",
     $           group_id_conf,error)
            datasetname='numaper'
            call hdf5write(group_id_conf,datasetname,numaperinc,dim)
            datasetname='numaper int'
            call hdf5write(group_id_conf,datasetname,numaperinc2,dim)
         endif

         dim(1)=1
         dim(2)=1
         datasetname='k0'
         call hdf5write(group_id_conf,datasetname,k0,dim)        
         datasetname='deltax'
         call hdf5write(group_id_conf,datasetname,aretecube,dim)
         datasetname='deltak'
         deltakx=2.d0*pi/dble(nfft2d)/aretecube
         call hdf5write(group_id_conf,datasetname,deltakx,dim)
         datasetname='nfft2d'
         call hdf5write1d_int(group_id_conf,datasetname,nfft2d,dim)
         datasetname='nfourier'
         imaxk0=nint(k0/deltak)+1
         k=2*imaxk0+1
         call hdf5write1d_int(group_id_conf,datasetname,k,dim)         
         datasetname='nbillu'
         call hdf5write1d_int(group_id_conf,datasetname,niter,dim)
         call h5gcreate_f(group_id_conf,"Fourier", group_id_conf_fourier
     $        ,error)
         call h5gcreate_f(group_id_conf,"Image", group_id_conf_image
     $        ,error)
#endif         
      endif


      
      niterii=0
      do ipol=1,npol
       
         if (npolainc.eq.1) then
            ss=1.d0
            pp=0.d0
         elseif (npolainc.eq.2) then
            ss=0.d0
            pp=1.d0
         else
            if (ipol.eq.1) then
               ss=1.d0
               pp=0.d0
            else
               ss=0.d0
               pp=1.d0
            endif
         endif

         
c     sommation 
         do idelta=-ideltam,ideltam
            do jdelta=-ideltam,ideltam

               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               i=ideltam+idelta+1
               j=ideltam+jdelta+1
               if (cdabs(masque(i+(j-1)*(2*ideltam+1))).le.2.d0) then
                  niterii=niterii+1
                  write(*,*) '*** incidence',niterii,'/',niter,' *****'
c     calcul champ incident
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere           
                     call ondeplanekxky(xs(i),ys(i),zs(i),k0,E0,ss,pp
     $                    ,kxinc,kyinc,FF0(3*i-2),FF0(3*i-1),FF0(3*i)
     $                    ,nstop,infostr)
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL

c     calcul champ local
                  if (nrig.eq.0) then
                     tol=tolinit
                     ncompte=0
                     nloop=0
                     if (nproche.eq.-1) then
                        call inverserig(FFTTENSORxx,FFTTENSORxy
     $                       ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                       ,FFTTENSORzz,vectx,vecty,vectz ,Tabdip
     $                       ,ntotalm,ntotal,ldabi,nlar,nmax,ndipole,nxm
     $                       ,nym ,nzm,nx,ny,nz,nx2,ny2,nxy2,nz2
     $                       ,nbsphere,nbsphere3,XI ,XR,wrk,FF,FF0,FFloc
     $                       ,polarisa,methodeit,tol,tol1,nloop,ncompte
     $                       ,nlim ,planf,planb,nstop,infostr)
                        if (nstop.eq.1) return
                     else

                        call inverserigopt(FFTTENSORxx,FFTTENSORxy
     $                       ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                       ,FFTTENSORzz,vectx,vecty,vectz,ntotalm
     $                       ,ntotal ,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny
     $                       ,nz,nx2,ny2 ,nxy2,nz2,nbsphere,nbsphere3,XI
     $                       ,XR,wrk,FF,FF0 ,FFloc,polarisa,methodeit
     $                       ,tol,tol1,nloop ,ncompte,nlim,planf,planb
     $                       ,nstop ,infostr)
                        if (nstop.eq.1) return
                     endif
                  elseif (nrig.eq.1) then 
c     **************************************        
c     Renormalized Born approximation field
c     **************************************
                     write(*,*) 'Renormalized Born approximation'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere3
                        FFloc(i)=FF0(i)
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere
                        k=3*(i-1)
                        do ii=1,3
                           FF(k+ii)=0.d0
                           do jj=1,3
                              FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                             *FFloc(k+jj)
                           enddo
                        enddo
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  elseif (nrig.eq.2) then 
c     **************************************        
c     Born approximation field
c     **************************************
                     write(*,*) 'Born approximation'
                     nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,kk,Em,Eloc,ii,jj,epsani)   
!$OMP DO SCHEDULE(STATIC) 
                     do k=1,nbsphere
                        kk=3*(k-1)
                        Em(1)=FF0(kk+1)
                        Em(2)=FF0(kk+2)
                        Em(3)=FF0(kk+3)
                        do ii=1,3
                           do jj=1,3
                              epsani(ii,jj)=epsilon(k,ii,jj)
                           enddo
                        enddo 
                        call local_macro(Eloc,Em,epsani,aretecube,k0
     $                       ,nsens)
                        FFloc(kk+1)=Eloc(1)
                        FFloc(kk+2)=Eloc(2)
                        FFloc(kk+3)=Eloc(3)
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

c     dipole
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere
                        k=3*(i-1)
                        do ii=1,3
                           FF(k+ii)=0.d0
                           do jj=1,3
                              FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                             *FFloc(k+jj)
                           enddo
                        enddo
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  elseif (nrig.eq.3) then
c     renormalized Born ordre 1
                     write(*,*) 'Born series order 1'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC) 
                     do i=1,nbsphere3
                        xr(i)=FF0(i)           
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)  
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere
                        k=3*(i-1)
                        xi(k+1)=-polarisa(i,1,1)*xr(k+1)-polarisa(i,1,2)
     $                       *xr(k+2)-polarisa(i,1,3)*xr(k+3)
                        xi(k+2)=-polarisa(i,2,1)*xr(k+1)-polarisa(i,2,2)
     $                       *xr(k+2)-polarisa(i,2,3)*xr(k+3)
                        xi(k+3)=-polarisa(i,3,1)*xr(k+1)-polarisa(i,3,2)
     $                       *xr(k+2)-polarisa(i,3,3)*xr(k+3)
                     enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                     
                     call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                    ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                    ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                    ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                    ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)       
                     do i=1,nbsphere
                        k=3*(i-1)
                        FFloc(k+1)=xr(k+1)
                        FFloc(k+2)=xr(k+2)
                        FFloc(k+3)=xr(k+3)
                        FF(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)
     $                       *xr(k+2)+polarisa(i,1,3)*xr(k+3)
                        FF(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)
     $                       *xr(k+2)+polarisa(i,2,3)*xr(k+3)
                        FF(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)
     $                       *xr(k+2)+polarisa(i,3,3)*xr(k+3)
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

                     
                  elseif (nrig.eq.4) then
c     Rytov renormalize
                     write(*,*) 'Rytov approximation on local field'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere3
                        xr(i)=0.d0       
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere
                        k=3*(i-1)
                        xi(k+1)=-polarisa(i,1,1)*FF0(k+1)-polarisa(i,1
     $                       ,2)*FF0(k+2)-polarisa(i,1,3)*FF0(k+3)
                        xi(k+2)=-polarisa(i,2,1)*FF0(k+1)-polarisa(i,2
     $                       ,2)*FF0(k+2)-polarisa(i,2,3)*FF0(k+3)
                        xi(k+3)=-polarisa(i,3,1)*FF0(k+1)-polarisa(i,3
     $                       ,2)*FF0(k+2)-polarisa(i,3,3)*FF0(k+3)
                     enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                     
                     call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                    ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                    ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                    ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                    ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
                     do i=1,nbsphere
                        k=3*(i-1)
                        tmp=dsqrt(dreal(FF0(k+1)*dconjg(FF0(k+1))+FF0(k
     $                       +2)*dconjg(FF0(k+2))+FF0(k+3)*dconjg(FF0(k
     $                       +3))))*1.d-6
                        if (cdabs(FF0(k+1)).le.tmp) then
                           FFloc(k+1)=0.d0
                        else
                           FFloc(k+1)=FF0(k+1)*cdexp(xr(k+1)/FF0(k+1))
                        endif
                        if (cdabs(FF0(k+2)).le.tmp) then
                           FFloc(k+2)=0.d0
                        else
                           FFloc(k+2)=FF0(k+2)*cdexp(xr(k+2)/FF0(k+2))
                        endif
                        if (cdabs(FF0(k+3)).le.tmp) then
                           FFloc(k+3)=0.d0
                        else
                           FFloc(k+3)=FF0(k+3)*cdexp(xr(k+3)/FF0(k+3))
                        endif
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
                     do i=1,nbsphere
                        k=3*(i-1)  
                        FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                       ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                        FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                       ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                        FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                       ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

                  elseif (nrig.eq.5) then
c     Rytov
                     write(*,*)
     $                    'Rytov approximation on macroscopic field'
c     a3=aretecube*aretecube*aretecube
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)   
!$OMP DO SCHEDULE(STATIC) 
                     do i=1,nbsphere
                        k=3*(i-1)
                        xr(k+1)=-FF0(k+1)*(epsilon(i,1,1)-eps0)/3.d0
                        xr(k+2)=-FF0(k+2)*(epsilon(i,2,2)-eps0)/3.d0
                        xr(k+3)=-FF0(k+3)*(epsilon(i,3,3)-eps0)/3.d0
                     enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                     nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,Em,Eloc,ii,jj,epsani)
!$OMP DO SCHEDULE(STATIC)
                     do i=1,nbsphere
                        k=3*(i-1)
                        Em(1)=FF0(k+1)
                        Em(2)=FF0(k+2)
                        Em(3)=FF0(k+3)
                        do ii=1,3
                           do jj=1,3
                              epsani(ii,jj)=epsilon(i,ii,jj)
                           enddo
                        enddo 
                        call local_macro(Eloc,Em,epsani,aretecube,k0
     $                       ,nsens)
                        FF(k+1)=Eloc(1)
                        FF(k+2)=Eloc(2)
                        FF(k+3)=Eloc(3)
                        xi(k+1)=-polarisa(i,1,1)*FF(k+1)-polarisa(i,1,2)
     $                       *FF(k+2)-polarisa(i,1,3)*FF(k+3)
                        xi(k+2)=-polarisa(i,2,1)*FF(k+1)-polarisa(i,2,2)
     $                       *FF(k+2)-polarisa(i,2,3)*FF(k+3)
                        xi(k+3)=-polarisa(i,3,1)*FF(k+1)-polarisa(i,3,2)
     $                       *FF(k+2)-polarisa(i,3,3)*FF(k+3)
                     enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                     
                     call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                    ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                    ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                    ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                    ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
                     do i=1,nbsphere
                        k=3*(i-1)
                        tmp=dsqrt(dreal(FF(k+1)*dconjg(FF(k+1))+FF(k+2)
     $                       *dconjg(FF(k+2))+FF(k+3)*dconjg(FF(k+3))))
     $                       *1.d -6
                        if (cdabs(FF(k+1)).le.tmp) then
                           FFloc(k+1)=0.d0
                        else
                           FFloc(k+1)=FF(k+1)*cdexp(xr(k+1)/FF(k+1))
                        endif
                        if (cdabs(FF(k+2)).le.tmp) then
                           FFloc(k+2)=0.d0
                        else
                           FFloc(k+2)=FF(k+2)*cdexp(xr(k+2)/FF(k+2))
                        endif
                        if (cdabs(FF(k+3)).le.tmp) then
                           FFloc(k+3)=0.d0
                        else
                           FFloc(k+3)=FF(k+3)*cdexp(xr(k+3)/FF(k+3))
                        endif
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)      
                     do i=1,nbsphere
                        k=3*(i-1)  
                        FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                       ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                        FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                       ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                        FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                       ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      
                     
                  elseif (nrig.eq.6) then
c     Beam propagation method
                     write(*,*) 'Beam propagation method'

                     call beampropagationmacro(xs,ys,zs,aretecube,k0,w0
     $                    ,E0,ss,pp,theta,phi,zero,zero,zero,beam
     $                    ,epsilon,ndipole ,nx ,ny,nz,nxm,nym ,nzm,nmax
     $                    ,nfft2d,Efourierx,Efouriery,Efourierz,FF0
     $                    ,FFloc,FF ,plan2f,plan2b ,nstop,infostr)

                     if (nstop.eq.1) return

                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
                     do i=1,nbsphere
                        k=3*(i-1)  
                        FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                       ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                        FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                       ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                        FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                       ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL    
                  elseif (nrig.eq.7) then
c     Beam propagation method
                     call beampropagation(xs,ys,zs,aretecube,k0,w0,E0,ss
     $                    ,pp,theta,phi,zero,zero,zero,beam,epsilon
     $                    ,ndipole,nx ,ny,nz,nxm,nym ,nzm,nmax,nfft2d
     $                    ,Efourierx,Efouriery,Efourierz,FF0 ,FFloc,FF
     $                    ,plan2f ,plan2b,nstop,infostr)

                     if (nstop.eq.1) return

                     
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
                     do i=1,nbsphere
                        k=3*(i-1)  
                        FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                       ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                        FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                       ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                        FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                       ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                     enddo            
!$OMP ENDDO 
!$OMP END PARALLEL    
                  endif
c     *********************************************************
c     fin calcul champ local
c     *********************************************************
c     *********************************************************            
c     calcul champ diffracte
c     *********************************************************

                  if (nquicklens.eq.1) then
                     
                     call diffractefft2dlens(nx,ny,nz,nxm,nym,nzm,nfft2d
     $                    ,tabfft2,k0,xs,ys,zs,aretecube,Efourierx
     $                    ,Efouriery,Efourierz,FF,imaxk0,deltakx,deltaky
     $                    ,Ediffkzpos,numaper,nside,plan2f,plan2b ,nstop
     $                    ,infostr)
                     if (nstop.eq.1) return
                  else
                     
                     
                     deltaky=deltakx
                     do i=-imaxk0,imaxk0
                        kx=deltakx*dble(i)
                        do j=-imaxk0,imaxk0
                           ky=deltaky*dble(j)
                           if (dsqrt(kx*kx+ky*ky).le.numaper) then
                              kz=dsqrt(k0*k0-kx*kx-ky*ky)
                              normal(1)=kx/k0
                              normal(2)=ky/k0
                              normal(3)=-dsqrt(1.d0-normal(1)*normal(1)
     $                             -normal(2)*normal(2))
                              Emx=0.d0
                              Emy=0.d0
                              Emz=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,kk,ctmp,ctmp1)
!$OMP DO SCHEDULE(STATIC) REDUCTION(+:Emx,Emy,Emz)
                              do ii=1,nbsphere
                                 kk=3*(ii-1)
                                 ctmp=cdexp(-icomp*k0*(normal(1)*xs(ii)
     $                                +normal(2)*ys(ii)+normal(3)
     $                                *zs(ii)))
                                 ctmp1=normal(1)*FF(kk+1)+normal(2)
     $                                *FF(kk+2)+normal(3)*FF(kk+3)
                                 Emx=Emx+ctmp*(FF(kk+1)-ctmp1*normal(1))
                                 Emy=Emy+ctmp*(FF(kk+2)-ctmp1*normal(2))
                                 Emz=Emz+ctmp*(FF(kk+3)-ctmp1*normal(3))
                              enddo
!$OMP ENDDO 
!$OMP END PARALLEL                     
                              ctmp=-2.d0*pi*icomp*kz                   
                              kk=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                              ii=imaxk0+i+1
                              jj=imaxk0+j+1
                              Ediffkzpos(ii,jj,1)=Emx*k0*k0/ctmp
                              Ediffkzpos(ii,jj,2)=Emy*k0*k0/ctmp
                              Ediffkzpos(ii,jj,3)=Emz*k0*k0/ctmp
                           endif
                        enddo
                     enddo
                  endif   
c     *********************************************************            
c     fin champ diffracte
c     *********************************************************
                  if (nmicsavefield.eq.1.or.nmicsavefield.eq.3) then
                     k=1
                     call integer_string4(niterii,fil4)
                     name='Nb_illumination'//fil4
                     do i=-imaxk0,imaxk0
                        do j=-imaxk0,imaxk0
                           ii=imaxk0+i+1
                           jj=imaxk0+j+1
                           kk=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                           Efourierx(kk)=Ediffkzpos(ii,jj,1)
                           Efouriery(kk)=Ediffkzpos(ii,jj,2)
                           Efourierz(kk)=Ediffkzpos(ii,jj,3)
                        enddo
                     enddo
                  
                     call writehdf5mic(Efourierx,Efouriery ,Efourierz
     $                    ,nfft2d,imaxk0,Ediffkzneg,k,name
     $                    ,group_id_conf_fourier)

                     dim(1)=3
                     dim(2)=3
                     datasetname='Nb_illumination'//fil4//'kinc'
                     u(1)=kxinc
                     u(2)=kyinc
                     u(3)=dsqrt(k0*k0-kxinc*kxinc-kyinc*kyinc)
                     call hdf5write1d(group_id_conf_fourier,datasetname
     $                    ,u,dim)

                     datasetname='Nb_illumination'//fil4//'uinc'
                     x=0.d0
                     y=0.d0
                     z=0.d0
                     call ondeplanekxky(x,y,z,k0,E0,ss,pp,kxinc,kyinc
     $                    ,Ex,Ey,Ez,nstop ,infostr)
                     u(1)=cdabs(Ex)
                     u(2)=cdabs(Ey)
                     u(3)=cdabs(Ez)
                     tmp=dsqrt(u(1)*u(1)+u(2)*u(2)+u(3)*u(3))
                     u(1)=u(1)/tmp
                     u(2)=u(2)/tmp
                     u(3)=u(3)/tmp
                     call hdf5write1d(group_id_conf_fourier,datasetname
     $                    ,u,dim)
                  
                  endif


                  
                  
c     *********************************************************            
c     calcul image
c     *********************************************************  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nfft2d*nfft2d
                     Efourierx(i)=0.d0
                     Efouriery(i)=0.d0
                     Efourierz(i)=0.d0                          
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL

                  call deltakroutine(kxinc,kyinc,deltak,deltak,k0
     $                 ,ikxinc,jkyinc)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indicex,indicey,indice)
!$OMP& PRIVATE(kx,ky,kz,u1,u2,normal,tmp,tmpx,tmpy,tmpz,ii,jj)
!$OMP& PRIVATE(phi,theta,Ex,Ey,Ez,zfocus,u,v,costmp,sintmp)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
                  do i=-imaxk0,imaxk0 
                     do j=-imaxk0,imaxk0
                        if (i.ge.0) then
                           indicex=i+1
                        else
                           indicex=nfft2d+i+1
                        endif
                        if (j.ge.0) then
                           indicey=j+1
                        else
                           indicey=nfft2d+j+1
                        endif
                        indice=indicex+nfft2d*(indicey-1)

                        kx=deltakx*dble(i)
                        ky=deltaky*dble(j)
                        if (kx*kx+ky*ky.le.numaper*numaper) then
                           kz=k0*k0-kx*kx-ky*ky
                           kz=dsqrt(kz)
                           zfocus=cdexp(icomp*kz*zlens)
                           u1=-ky/k0
                           u2=kx/k0
                           tmp=dsqrt(u1*u1+u2*u2)

                           ii=imaxk0+i+1
                           jj=imaxk0+j+1
                           
                           Efourierx(indice)=Ediffkzpos(ii,jj,1)*zfocus
                           Efouriery(indice)=Ediffkzpos(ii,jj,2)*zfocus
                           Efourierz(indice)=Ediffkzpos(ii,jj,3)*zfocus

                           
                           if (tmp.ne.0.d0) then    
                              u(1)=kx/k0
                              u(2)=ky/k0
                              u(3)=dsqrt(1.d0-u(1)*u(1)-u(2)*u(2))
     $                             *dble(nside)                              
                              v(1)=-kx/k0/gross
                              v(2)=-ky/k0/gross
                              v(3)=dsqrt(1.d0-v(1)*v(1)-v(2)*v(2))
     $                             *dble(nside)
                              u1=u(2)*v(3)-u(3)*v(2)
                              u2=-u(1)*v(3)+u(3)*v(1)
                              costmp=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
                              sintmp=dsqrt(u1*u1+u2*u2)                
                              u1=u1/sintmp
                              u2=u2/sintmp
                              tmp=dsqrt(u(3)/v(3))
                              tmpx=(u1*u1+(1.d0-u1*u1)*costmp)
     $                             *Efourierx(indice) +u1*u2*(1.d0
     $                             -costmp) *Efouriery(indice)+u2
     $                             *sintmp*Efourierz(indice)
                              tmpy=u1*u2*(1.d0-costmp)*Efourierx(indice)
     $                             +(u2*u2+(1.d0-u2*u2)*costmp)
     $                             *Efouriery(indice)-u1*sintmp
     $                             *Efourierz(indice)
                              tmpz=-u2*sintmp*Efourierx(indice)+u1
     $                             *sintmp*Efouriery(indice)+costmp
     $                             *Efourierz(indice)
                              Efourierx(indice)=tmpx*tmp
                              Efouriery(indice)=tmpy*tmp
                              Efourierz(indice)=tmpz*tmp
                           endif
                        endif
                     enddo
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL

                  call fouriertoimage2(deltakx,deltaky,gross,Efourierx
     $                 ,Efouriery,Efourierz,nfft2D,nfft2d2,plan2b
     $                 ,plan2f)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(indice)   
!$OMP DO SCHEDULE(STATIC)    
                  do indice=1,nfft2d*nfft2d
                     Eimagex(indice)=Efourierx(indice)
     $                    *dconjg(Efourierx(indice))+Eimagex(indice)
                     Eimagey(indice)=Efouriery(indice)
     $                    *dconjg(Efouriery(indice))+Eimagey(indice)
                     Eimagez(indice)=Efourierz(indice)
     $                    *dconjg(Efourierz(indice))+Eimagez(indice)
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL

               
c     sauvegarde les champs pour chaque incidence dans le plan image
                  if (nmicsavefield.ge.2) then
                     write(*,*) 'write the image data'
                     k=0
                     j=0
                     call integer_string4(niterii,fil4)
                     name='Nb_illumination'//fil4
                     call writehdf5mic(Efourierx,Efouriery ,Efourierz
     $                    ,nfft2d,j,Ediffkzpos,k,name
     $                    ,group_id_conf_image)
                     
                     dim(1)=3
                     dim(2)=3
                     datasetname='Nb_illumination'//fil4//'kinc'
                     u(1)=kxinc
                     u(2)=kyinc
                     u(3)=dsqrt(k0*k0-kxinc*kxinc-kyinc*kyinc)
                     call hdf5write1d(group_id_conf_image,datasetname,u,
     $                    dim)
                     
                     datasetname='Nb_illumination'//fil4//'uinc'
                     x=0.d0
                     y=0.d0
                     z=0.d0
                     call ondeplanekxky(x,y,z,k0,E0,ss,pp,kxinc,kyinc
     $                    ,Ex,Ey,Ez,nstop ,infostr)
                     u(1)=cdabs(Ex)
                     u(2)=cdabs(Ey)
                     u(3)=cdabs(Ez)
                     tmp=dsqrt(u(1)*u(1)+u(2)*u(2)+u(3)*u(3))
                     u(1)=u(1)/tmp
                     u(2)=u(2)/tmp
                     u(3)=u(3)/tmp
                     call hdf5write1d(group_id_conf_image,datasetname,u,
     $                    dim)
                     
                  endif       

               endif
               if (nstop.eq.-1) then
                  infostr =
     $              'Calculation cancelled during iterative method'
                  if (nmicsavefield.ge.1) then
#ifdef USE_HDF5                      
                     CALL h5gclose_f(group_id_conf,error)
                     CALL h5gclose_f(group_id_conf_fourier,error)
                     CALL h5gclose_f(group_id_conf_image,error)      
                     call h5fclose_f(file_idmicros,error)
                     file_idmicros=0
#endif                        
                  endif               
                  return
               endif
              
            enddo
         enddo
      enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(indice)   
!$OMP DO SCHEDULE(STATIC)        
      do indice=1,nfft2d*nfft2d
         Eimagex(indice)=cdsqrt(Eimagex(indice))
         Eimagey(indice)=cdsqrt(Eimagey(indice))
         Eimagez(indice)=cdsqrt(Eimagez(indice))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      deltax=2.d0*pi/dble(nfft2d)/deltakx

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
      do i=-nfft2d2,nfft2d2-1
         xy(i+nfft2d2+1)=deltax*dble(i)*gross
         kxy(i+nfft2d2+1)=deltakx*dble(i)/k0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      
      if (nmat.eq.0) then
         open(400,file='kxfourier.mat')
         open(401,file='ximage.mat')
         do i=-nfft2d2,nfft2d2-1
            write(401,*) xy(i+nfft2d2+1)
         enddo
         do i=-imaxk0,imaxk0
            kx=deltakx*dble(i)
            write(400,*) kx/k0
         enddo
         close(400)
         close(401)

         open(310,file='kxincidentbf.mat')
         open(311,file='kyincidentbf.mat')
         do idelta=-ideltam,ideltam
            do jdelta=-ideltam,ideltam
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               if (kxinc*kxinc+kyinc*kyinc.le.numaperk*numaperk) then
                  write(310,*) kxinc/k0
                  write(311,*) kyinc/k0
               endif
            enddo
         enddo
         close (310)
         close (311)

         open(301,file='imagebf.mat')
         open(302,file='imagebfx.mat')
         open(303,file='imagebfy.mat')
         open(304,file='imagebfz.mat')

         do i=1,nfft2D*nfft2D
            write(301,*) dsqrt(dreal(Eimagex(i)**2.d0+Eimagey(i)**2.d0
     $           +Eimagez(i)**2.d0))
            write(302,*) dreal(Eimagex(i))
            write(303,*) dreal(Eimagey(i))
            write(304,*) dreal(Eimagez(i))
         enddo
         close(301)
         close(302)
         close(303)
         close(304)

         open(310,file='kxmic.mat')
         open(311,file='kymic.mat')
         open(312,file='masquemic.mat')         
         do jdelta=-ideltam,ideltam
            do idelta=-ideltam,ideltam          
               kxinc=dble(idelta)*deltak
               kyinc=dble(jdelta)*deltak
               i=ideltam+idelta+1
               j=ideltam+jdelta+1
               if (jdelta.eq.0) write(310,*) kxinc/k0
               if (idelta.eq.0) write(311,*) kyinc/k0
               tmp=cdabs(masque(i+(j-1)*(2*ideltam+1)))
               write(312,*) tmp
            enddo
         enddo
         close (310)
         close (311)
         close (312)

         
      elseif (nmat.eq.2) then
         dim(1)=nfft2d
         dim(2)=nfft2d
         datasetname='x Image'
         call hdf5write1d(group_idmic,datasetname,xy,dim)
         k=0
         datasetname='Image bright field kz<0'
         call writehdf5mic(Eimagex,Eimagey,Eimagez,nfft2d ,imaxk0
     $        ,Ediffkzpos,k,datasetname,group_idmic)

         ii=0
         do idelta=-ideltam,ideltam
            do jdelta=-ideltam,ideltam
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               if (kxinc*kxinc+kyinc*kyinc.le.numaperk*numaperk) then
                  ii=ii+1
                  kxy(ii)=kxinc/k0
               endif
            enddo
         enddo
         dim(1)=ii
         dim(2)=nfft2d
         datasetname='kx incident bf'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         
         ii=0
         do idelta=-ideltam,ideltam
            do jdelta=-ideltam,ideltam
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               if (kxinc*kxinc+kyinc*kyinc.le.numaperk*numaperk) then
                  ii=ii+1
                  kxy(ii)=kyinc/k0
               endif
            enddo
         enddo
         dim(1)=ii
         dim(2)=nfft2d
         datasetname='ky incident bf'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)

         do idelta=-ideltam,ideltam
            kxy(idelta+ideltam+1)=deltak*dble(idelta)/k0
         enddo
         dim(1)=ideltam*2+1
         dim(2)=nfft2d
         datasetname='kxmic'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         datasetname='kymic'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         dim(1)=(ideltam*2+1)**2
         dim(2)=nmasque*nmasque
         dim(3)=1
         dim(4)=1
         datasetname='masquemic'
         do jdelta=-ideltam,ideltam
            do idelta=-ideltam,ideltam
               i=ideltam+idelta+1
               j=ideltam+jdelta+1
               masque(i+(j-1)*(2*ideltam+1))=cdabs(masque(i+(j-1)*(2
     $              *ideltam+1)))*(1.d0,0.d0)
            enddo
         enddo
         call hdf5write1d(group_idmic,datasetname,dreal(masque),dim)
         
      endif
      
      end
      
