#include "cdmOptionsWindow.h"
#include "cdmMain.h"

OptionsWindow::OptionsWindow( QMainWindow *_parent, Options *_options, Qt::WindowFlags fl)
  : QMainWindow( _parent, fl )
{
  parent = _parent;
  optionswidget = NULL;
  optionstable = NULL;
  options = _options;
  
  QWidget *centralWidget = new QWidget(this);
  this->setCentralWidget(centralWidget);
  vboxlayout = new QVBoxLayout();
  centralWidget->setLayout(vboxlayout);
  this->setWindowTitle("Configurations saved");
  this->setMinimumHeight(300);
  this->setMinimumWidth(400);
  
  optionstitle = new QLabel("Select a configuration:");
  vboxlayout->addWidget(optionstitle);
  optionsview = new QTableView();
  QPalette palette = optionsview->palette();
  palette.setBrush(QPalette::Base, Qt::transparent);
  optionsview->setPalette(palette);
  optionsview->setItemDelegate(new ColorDelegate());
  optionsview->setEditTriggers(QAbstractItemView::AllEditTriggers);
  //optionsview->setSelectionBehavior(QAbstractItemView::SelectRows);
  vboxlayout->addWidget(optionsview);

  saveButton = new QPushButton("Save",this);
  saveButton->setToolTip("Enter a new name or select a line to save the current calculation");
  saveButton->setFixedHeight(30);
  saveButton->setFixedWidth(150);
  QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
  vboxlayout->addWidget(saveButton);
  removeButton = new QPushButton("Delete",this);
  removeButton->setToolTip("Select a line to delete");
  removeButton->setFixedHeight(30);
  removeButton->setFixedWidth(150);
  QObject::connect(removeButton, SIGNAL(clicked()), this, SLOT(remove()));
  vboxlayout->addWidget(removeButton);
  loadButton = new QPushButton("Load",this);
  loadButton->setToolTip("Select a line to load");
  loadButton->setFixedHeight(30);
  loadButton->setFixedWidth(150);
  QObject::connect(loadButton, SIGNAL(clicked()), this, SLOT(load()));
  vboxlayout->addWidget(loadButton);
  exportButton = new QPushButton("Export",this);
  exportButton->setToolTip("Select a line to export");
  exportButton->setFixedHeight(30);
  exportButton->setFixedWidth(150);
  QObject::connect(exportButton, SIGNAL(clicked()), this, SLOT(tofile()));
  vboxlayout->addWidget(exportButton);
  initOptionsTbl();
} 

OptionsWindow::~OptionsWindow()
{
  QLOG_DEBUG ( ) << QString("Deleting OptionsWindow");
  if (optionstable){ delete optionstable; optionstable = NULL;}
}

void OptionsWindow::closeEvent(QCloseEvent* event)
{
  event->accept();  
  QLOG_DEBUG ( ) << QString("Closing OptionsWindow");
  this->hide();
}
void OptionsWindow::initOptionsTbl() {
  QTabWidget *tabwidget = NULL;
  QMainWindow *currentWindow = NULL;
  tabwidget = (QTabWidget*) parent->findChild<QTabWidget*>("TabWidget");
  if (tabwidget) {
   currentWindow = (QMainWindow*)tabwidget->currentWidget();
   optionswidget = currentWindow->findChild<OptionsWidget *>("Options");
  }
  /*if (optionswidget)
    options = optionswidget->options;
  else {
   if (options) {delete options; options = NULL;}
   options = new Options();
   options->initDb();
  } */
  if (optionstable) {delete optionstable; optionstable = NULL;}
  dbpath = "options.db3";
  optionstable = new ManifestModel(this,QSqlDatabase::database(dbpath));
  optionstable->setTable("options_tbl");
  optionstable->setEditStrategy(QSqlTableModel::OnManualSubmit);
  optionsview->setModel(optionstable);
  optionstable->setFilter("name not like 'new'");
  optionstable->select();
  optionstable->insertRow(optionstable->rowCount());
  optionsview->resizeColumnsToContents();
  optionsview->resizeRowsToContents();
  optionsview->setCurrentIndex (optionsview->model()->index(optionstable->rowCount() - 2, 0, QModelIndex()));
}
void 
OptionsWindow::save(){
  if (optionsview->selectionModel()->hasSelection() == false) {
     this->showWarning("Select a valid configuration to save");
     return;
  }
  QModelIndex selected = optionsview->selectionModel()->currentIndex();
  QSqlRecord record = optionstable->record(selected.row());
  QString name = record.value(0).toString();
  QString description = record.value(1).toString();
  if (name =="") {
   this->showWarning("Select a valid configuration to save");
   return;
  }
  QTabWidget *tabwidget = (QTabWidget*) parent->findChild<QTabWidget*>("TabWidget");
  QMainWindow *currentWindow = (QMainWindow*)tabwidget->currentWidget();
  optionswidget = currentWindow->findChild<OptionsWidget *>("Options");
  if (optionswidget)
    optionswidget->updateOptions();
  else
    options->loadDb(name);
  options->saveDb(name,description);
  initOptionsTbl();
}
void 
OptionsWindow::load(){
  if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to load");
  return;
  }
  QModelIndexList selectedlist = optionsview->selectionModel()->selectedIndexes();
  for (int i = selectedlist.size() - 1 ; i >=0 ; i--) {
    QModelIndex selected = selectedlist.at(i);
    QSqlRecord record = optionstable->record(selected.row());
    QString name = record.value(0).toString();
    options->loadDb(name);
    CdmMain *mainwindow = (CdmMain*) parent; 
    mainwindow->openWidget(options);
 }
 this->hide();
}
void 
OptionsWindow::remove(){
  if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to delete");
    return;
  }
  QModelIndexList selectedlist = optionsview->selectionModel()->selectedIndexes();
  for (int i = selectedlist.size() - 1 ; i >=0 ; i--) {
    QModelIndex selected = selectedlist.at(i);
    QSqlRecord record = optionstable->record(selected.row());
    QString name = record.value(0).toString();
    QString description = record.value(1).toString();
    if (name =="") {
      this->showWarning("Select a valid configuration to delete");
      return;
    }
    options->removeDb(name);
  }
  initOptionsTbl();
}
void 
OptionsWindow::updatewindow(){
  initOptionsTbl();
}
void 
OptionsWindow::tofile(){
 if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to export");
    return;
  }
  QModelIndex selected = optionsview->selectionModel()->currentIndex();
  QSqlRecord record = optionstable->record(selected.row());
  QString name = record.value(0).toString();
  if (name =="") {
   this->showWarning("Select a valid configuration to export");
   return;
  }
  options->loadDb(name);
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                     name + ".opt", tr("Options (*.opt)"));
  QFile optfile(fileName);
  if (!optfile.open(QIODevice::WriteOnly | QIODevice::Text))
     return;
  QTextStream opt(&optfile);
  // Fill ASCII options here
  opt << QString("List of the options taken for the code");


  opt << QString("Calculation options [0=rigorous, 1=renormalized Born, 2=Born, 3=Born order, 4=renormalized Rytov, 5=Rytov, 6=Beam propagation, 7=renormalized Beam propagation]: ") << options->getNrig();

  opt << QString("Reread from a file: ") << options->getNread();
  opt << QString("Database file: ") << options->getNmatlab();
  opt << QString("H5 file used: ") << options->getH5File();
  opt << QString("Advanced interface used: ") << options->getAdvancedinterface() ;
  opt << QString("------------------------------------------") ;
  opt << QString("Illumination properties options: ") ;
  opt << QString(" Wavelength: ") << options->getWavelength() ;
  opt << QString(" P0: ") << options->getP0() ;
  opt << QString(" W0: ") << options->getW0() ;
  opt << QString(" Beam: ") << options->getBeam() ;
  if (options->getBeam() == "Circular plane wave") {
    opt << QString("  incidence angle (theta with respect to z): ") << options->getIncidenceangle_theta_z() ;
    opt << QString("  incidence angle (phi with respect to x): ") << options->getIncidenceangle_phi_x() ;
    opt << QString("  polarization L (-1) R (1): ") << options->getPolarizationRL() ;
  }
  else if (options->getBeam() == "Linear plane wave") {
    opt << QString("  incidence angle (theta with respect to z): ") << options->getIncidenceangle_theta_z() ;
    opt << QString("  incidence angle (phi with respect to x): ") << options->getIncidenceangle_phi_x() ;
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
  }
  else if (options->getBeam() == "Circular Gaussian" || options->getBeam() == "Circular Gaussian (para)"|| options->getBeam() == "Circular Gaussian (FFT)" ) {
    opt << QString("  incidence angle (theta with respect to z): ") << options->getIncidenceangle_theta_z() ;
    opt << QString("  incidence angle (phi with respect to x): ") << options->getIncidenceangle_phi_x() ;
    opt << QString("  polarization L (-1) R (1): ") << options->getPolarizationRL() ;
    opt << QString("  gaussian X: ") << options->getXgaus() ;
    opt << QString("  gaussian Y: ") << options->getYgaus() ;
    opt << QString("  gaussian Z: ") << options->getZgaus() ;
  }
  else if (options->getBeam() == "Linear Gaussian" || options->getBeam() == "Linear Gaussian (para)" || options->getBeam() == "Linear Gaussian (FFT)" ) {
    opt << QString("  incidence angle (theta with respect to z): ") << options->getIncidenceangle_theta_z() ;
    opt << QString("  incidence angle (phi with respect to x): ") << options->getIncidenceangle_phi_x() ;
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
    opt << QString("  gaussian X: ") << options->getXgaus() ;
    opt << QString("  gaussian Y: ") << options->getYgaus() ;
    opt << QString("  gaussian Z: ") << options->getZgaus() ;
  }
  else if (options->getBeam() == "Multiple wave"  ) {
    for (int i = 0 ; i < options->getWaveMultiNumber(); i++) {
      opt << QString("  incidence angle (theta with respect to z): ") << options->getThetam().at(i) ;
      opt << QString("  incidence angle (phi with respect to x): ") << options->getPhim().at(i) ;
      opt << QString("  polarization TM (1) TE (0): ") << options->getPpm().at(i) ;
      opt << QString("  Magnitude field real: ") << (real(options->getE0m().at(i))) ;
      opt << QString("  Magnitude field imag: ") << (imag(options->getE0m().at(i))) ;
    }
  }
  else if (options->getBeam() == "Antenna" ) {
    opt << QString("  incidence angle (theta with respect to z): ") << options->getIncidenceangle_theta_z() ;
    opt << QString("  orientation angle (phi with respect to x)") << options->getIncidenceangle_phi_x() ;
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
  }
  else if (options->getBeam() == "Green s tensor" ) {
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
  }
  else if (options->getBeam() == "Speckle" ) {
    opt << QString("  Seed ") << options->getSpeckseed() ; 
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
    opt << QString("  NA: ") << options->getNAil() ;
  }
  else if (options->getBeam() == "Speckle kz>0" ) {
    opt << QString("  Seed ") << options->getSpeckseed() ; 
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
    opt << QString("  NA: ") << options->getNAil() ;    
  }
 else if (options->getBeam() == "Confocal" ) {
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
    opt << QString("  NA: ") << options->getNAil() ;    
  }
  else if (options->getBeam() == "Confocal kz>0" ) {
    opt << QString("  polarization TM (1) TE (0): ") << options->getPolarizationTM() ;
    opt << QString("  position X: ") << options->getXgaus() ;
    opt << QString("  position Y: ") << options->getYgaus() ;
    opt << QString("  position Z: ") << options->getZgaus() ;
    opt << QString("  NA: ") << options->getNAil() ;    
  }  
  opt << QString("------------------------------------------") ;
  opt << QString("Object properties options: ") ;
  for (int i = 0 ; i < options->getObjectNumber(); i++) {
    opt << QString(" object : ") << i << QString(" : ") << options->getObject() ;
    if (options->getObject() == "sphere" || options->getObject() == "multiple spheres") {
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "inhomogeneous sphere") {
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      opt << QString("  seed: ") << options->getSphereseed() ;
      opt << QString("  coherence length: ") << options->getSpherecoherencelength() ;
      opt << QString("  standard deviation: ") << options->getSpherestandardev() ;
    }
    else if (options->getObject() == "random spheres (length)") {
      opt << QString("  cube side X: ") << options->getCubesidex() ;
      opt << QString("  cube side Y: ") << options->getCubesidey() ;
      opt << QString("  cube side Z: ") << options->getCubesidez() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      opt << QString("  seed: ") << options->getSphereseed() ;
      opt << QString("  density: ") << options->getDensity() ;
     }
    else if (options->getObject() == "random spheres (meshsize)") {
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  number of subunit X: ") << options->getNxx() ;
      opt << QString("  number of subunit Y: ") << options->getNyy() ;
      opt << QString("  number of subunit Z: ") << options->getNzz() ;
      opt << QString("  meshsize: ") << options->getMeshsize() ;      
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      opt << QString("  seed: ") << options->getSphereseed() ;
      opt << QString("  density: ") << options->getDensity() ;
     }
    else if (options->getObject() == "concentric spheres") {
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      if (i == 0) {
        opt << QString("  position X: ") << options->getPositionx().at(i) ;
        opt << QString("  position Y: ") << options->getPositiony().at(i) ;
        opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      }
    }
    else if (options->getObject() == "cube") {
      opt << QString("  cube side: ") << options->getCubeside() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "inhomogeneous cuboid (length)") {
      opt << QString("  cube side X: ") << options->getCubesidex() ;
      opt << QString("  cube side Y: ") << options->getCubesidey() ;
      opt << QString("  cube side Z: ") << options->getCubesidez() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  seed: ") << options->getSphereseed() ;
      opt << QString("  coherence length: ") << options->getSpherecoherencelength() ;
      opt << QString("  standard deviation: ") << options->getSpherestandardev() ;
    }
       else if (options->getObject() == "inhomogeneous cuboid (meshsize)") {
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  number of subunit X: ") << options->getNxx() ;
      opt << QString("  number of subunit Y: ") << options->getNyy() ;
      opt << QString("  number of subunit Z: ") << options->getNzz() ;
      opt << QString("  meshsize: ") << options->getMeshsize() ;
      opt << QString("  seed: ") << options->getSphereseed() ;
      opt << QString("  coherence length: ") << options->getSpherecoherencelength() ;
      opt << QString("  standard deviation: ") << options->getSpherestandardev() ;
    }
      else if (options->getObject() == "cuboid (length)") {
      opt << QString("  cube side X: ") << options->getCubesidex() ;
      opt << QString("  cube side Y: ") << options->getCubesidey() ;
      opt << QString("  cube side Z: ") << options->getCubesidez() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  theta: ") << options->getThetaobj() ;
      opt << QString("  phi: ") << options->getPhiobj() ;
      opt << QString("  psi: ") << options->getPsiobj() ;
    }
      else if (options->getObject() == "cuboid (meshsize)") {
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  number of subunit X: ") << options->getNxx() ;
      opt << QString("  number of subunit Y: ") << options->getNyy() ;
      opt << QString("  number of subunit Z: ") << options->getNzz() ;
      opt << QString("  meshsize: ") << options->getMeshsize() ;      
    }
    else if (options->getObject() == "ellipsoid") {
      opt << QString("  half axe A: ") << options->getDemiaxea() ;
      opt << QString("  half axe B: ") << options->getDemiaxeb() ;
      opt << QString("  half axe C: ") << options->getDemiaxec() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "cylinder") {
      opt << QString("  radius: ") << options->getSphereradius().at(i) ;
      opt << QString("  height: ") << options->getHauteur() ;
      opt << QString("  position X: ") << options->getPositionx().at(i) ;
      opt << QString("  position Y: ") << options->getPositiony().at(i) ;
      opt << QString("  position Z: ") << options->getPositionz().at(i) ;
      opt << QString("  theta: ") << options->getThetaobj() ;
      opt << QString("  phi: ") << options->getPhiobj() ;
    }
  }
    opt << QString(" anisotropy: ") << options->getAnisotropy() ;
  for (int i = 0 ; i < options->getObjectNumber(); i++) {
    if ( options->getAnisotropy() == "iso" ) {
      opt << QString("  material") << i << QString(": ") << options->getMaterial()[i] ;
      opt << QString("  epsilon real: ") << QString::number(real(options->getEpsilon().at(i))) ;
      opt << QString("  epsilon imag: ") << QString::number(imag(options->getEpsilon().at(i))) ;
    }
    else if ( options->getAnisotropy() == "ani" ) {
      opt << QString("  epsilon11 real: ") << QString::number(real(options->getEpsilon11().at(i))) ;
      opt << QString("  epsilon11 imag: ") << QString::number(imag(options->getEpsilon11().at(i))) ;
      opt << QString("  epsilon12 real: ") << QString::number(real(options->getEpsilon12().at(i))) ;
      opt << QString("  epsilon12 imag: ") << QString::number(imag(options->getEpsilon12().at(i))) ;
      opt << QString("  epsilon13 real: ") << QString::number(real(options->getEpsilon13().at(i))) ;
      opt << QString("  epsilon13 imag: ") << QString::number(imag(options->getEpsilon13().at(i))) ;
      opt << QString("  epsilon21 real: ") << QString::number(real(options->getEpsilon21().at(i))) ;
      opt << QString("  epsilon21 imag: ") << QString::number(imag(options->getEpsilon21().at(i))) ;
      opt << QString("  epsilon22 real: ") << QString::number(real(options->getEpsilon22().at(i))) ;
      opt << QString("  epsilon22 imag: ") << QString::number(imag(options->getEpsilon22().at(i))) ;
      opt << QString("  epsilon23 real: ") << QString::number(real(options->getEpsilon23().at(i))) ;
      opt << QString("  epsilon23 imag: ") << QString::number(imag(options->getEpsilon23().at(i))) ;
      opt << QString("  epsilon31 real: ") << QString::number(real(options->getEpsilon31().at(i))) ;
      opt << QString("  epsilon31 imag: ") << QString::number(imag(options->getEpsilon31().at(i))) ;
      opt << QString("  epsilon32 real: ") << QString::number(real(options->getEpsilon32().at(i))) ;
      opt << QString("  epsilon32 imag: ") << QString::number(imag(options->getEpsilon32().at(i))) ;
      opt << QString("  epsilon33 real: ") << QString::number(real(options->getEpsilon33().at(i))) ;
      opt << QString("  epsilon33 imag: ") << QString::number(imag(options->getEpsilon33().at(i))) ;
    }
  }
  opt << QString("------------------------------------------") ;
  opt << QString("Study options: ") ;
  opt << QString(" Dipole/epsilon checked: ") << options->getDipolepsilon() ;
  opt << QString(" Farfield checked: ") << options->getFarfield() ;
  if ( options->getFarfield() ) {
    opt << QString("  cross section checked: ") << options->getCrosssection() ;
    opt << QString("  cross section + poynting checked: ") << options->getCrosssectionpoynting() ;
    opt << QString("  quick computation: ") << options->getQuickdiffract() ;
    opt << QString("  ntheta: ") << options->getNtheta() ;
    opt << QString("  nphi: ") << options->getNphi() ;
    opt << QString("  Emissivity: ") << options->getNenergie() ;
  }
  opt << QString(" Microscopy checked: ") << options->getMicroscopy() ;
  if ( options->getMicroscopy() ) {
    opt << QString("Microscope [0=Holographic, 1=Brightfield, 2=Darkfield & phase, 3=Schieren, 4=Darkfield cone & phase, 5=Phase experimental, 6=Confocal]: ") << options->getNtypemic() ;
    opt << QString("  Side of observation [0 trans, 1 ref.]: ") << options->getNside() ;
    opt << QString("  Quick computation: ") << options->getMicroscopyFFT() ;
    opt << QString("  Numerical aperture lens: ") << options->getNA() ;
    opt << QString("  Numerical aperture condenser: ") << options->getNAinc() ;
    opt << QString("  Numerical aperture condenser: ") << options->getNAinc2() ;
    opt << QString("  Numerical aperture condenser: ") << options->getKcnax() ;
    opt << QString("  Numerical aperture condenser: ") << options->getKcnay() ;
    opt << QString("  Magnification: ") << options->getGross() ;
    opt << QString("  Focal plane position: ") << options->getZlens() ;
    opt << QString("  Psi for confocal: ") << options->getPsiinc() ;    
  }
  opt << QString(" Force checked: ") << options->getForce() ;
  if ( options->getForce() ) {
    opt << QString("  optical force checked: ") << options->getOpticalforce() ;
    opt << QString("  optical force density checked: ") << options->getOpticalforcedensity() ;
    opt << QString("  optical torque checked: ") << options->getOpticaltorque() ;
    opt << QString("  optical torque density checked: ") << options->getOpticaltorquedensity() ;
  }
  opt << QString(" Nearfield checked: ") << options->getNearfield() ;
  if ( options->getNearfield() ) {
    opt << QString("  localfield checked: ") << options->getLocalfield() ;
    opt << QString("  macroscopic field checked: ") << options->getMacroscopicfield() ;
    int nproche = options->getNproche();
    opt << QString("  range of study (0,1,2): ") << options->getNproche() ;
    opt << QString("  Discretization: ") << options->getDiscretization() ;
    opt << QString("  nxm: ") << options->getNxm() ;
    opt << QString("  nym: ") << options->getNym() ;
    opt << QString("  nzm: ") << options->getNzm() ;
    opt << QString("  nxmp: ") << options->getNxmp() ;
    opt << QString("  nymp: ") << options->getNymp() ;
    opt << QString("  nzmp: ") << options->getNzmp() ;
  }
  opt << QString("------------------------------------------") ;
  opt << QString("Numerical parameters options: ") ;
  opt << QString(" Maximum of iteration: ") << options->getNlim() ;
  opt << QString(" Tolerance: ") << options->getTolerance() ;
  opt << QString(" Methode: ") << options->getMethodeit() ;
  opt << QString(" Preconditionner: ") << options->getPrecon() ;
  opt << QString(" Initial guess: ") << options->getNinitest() ;
  opt << QString(" Polarizability: ") << options->getPolarizability() ;
  opt << QString(" Quadrature: ") << options->getQuad() ;
  opt << QString(" FFT: ") << options->getnfft2d() ;
  optfile.close();
  initOptionsTbl();
}
void 
OptionsWindow::showWarning(QString message) {
  QMessageBox::warning(this, "Error: ", message);
}

