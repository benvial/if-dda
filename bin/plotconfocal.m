function plotconfocal(hinci,event,nx,ny,nz,x,y,z,imageconf,imageconfx,imageconfy,imageconfz,nprint)
val = get(hinci,'Value');


switch val

    case 2

        figure(500)
        set(500,'DefaultAxesFontName','Times')
        set(0,'DefaultAxesFontSize',12)
        set(500,'DefaultAxesFontWeight','Bold')
        set(500,'DefaultTextfontName','Times')
        set(500,'DefaultTextfontSize',12)
        set(500,'DefaultTextfontWeight','Bold')
        set(500,'Position',[0 0 1000 600])

        uicontrol('Style', 'text', 'String', 'Choice of z','Position', [250 10 90 20]);
        uicontrol('Style', 'text', 'String', 'm','Position', [640 10 15 20]);
        uicontrol('Style', 'text', 'String', num2str(z(1),'%+1.2e\n'),'Position', [560 10 80 20]);

        subplot('position',[0.075 0.63 0.33 0.33])
        imagesc(x,y,imageconf(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('modulus')
        colorbar('vert')


        subplot('position',[0.65 0.63 0.33 0.33])
        imagesc(x,y,abs(imageconfx(:,:,1))');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title(' x')
        colorbar('vert')

        subplot('position',[0.075 0.15 0.33 0.33])
        imagesc(x,y,abs(imageconfy(:,:,1))');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title(' y')
        colorbar('vert')

        subplot('position',[0.65 0.15 0.33 0.33])
        imagesc(x,y,abs(imageconfz(:,:,1))');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title(' z')
        colorbar('vert')

	if (nz~=1)
          uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      		    'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    		    'Position', [350 10 200 30],'Callback', {@plotconfocalz,x,y,z,imageconf,imageconfx,imageconfy,imageconfz,nprint});
	else
	  printfig(nprint,'confocal')
	end
    case 3


        figure(500)
        set(500,'DefaultAxesFontName','Times')
        set(500,'DefaultAxesFontSize',12)
        set(500,'DefaultAxesFontWeight','Bold')
        set(500,'DefaultTextfontName','Times')
        set(500,'DefaultTextfontSize',12)
        set(500,'DefaultTextfontWeight','Bold')
        set(500,'Position',[0 0 1000 600])

        uicontrol('Style', 'text', 'String', 'Choice of y','Position', [250 10 90 20]);
        uicontrol('Style', 'text', 'String', 'm','Position', [640 10 15 20]);
        uicontrol('Style', 'text', 'String', num2str(y(1),'%+1.2e\n'),'Position', [560 10 80 20]);


        subplot('position',[0.075 0.63 0.33 0.33])
        imagesc(x,z,permute(imageconf(:,1,:),[1 3 2])');
        axis xy

        shading interp
        xlabel('x')
        ylabel('z')
        axis image
        title('modulus')
        colorbar('vert')


        subplot('position',[0.65 0.63 0.33 0.33])
        imagesc(x,z,permute(abs(imageconfx(:,1,:)),[1 3 2])');
        axis xy

        shading interp
        xlabel('x')
        ylabel('z')
        axis image
        title(' x')
        colorbar('vert')

        subplot('position',[0.075 0.15 0.33 0.33])
        imagesc(x,z,permute(abs(imageconfy(:,1,:)),[1 3 2])');
        axis xy

        shading interp
        xlabel('x')
        ylabel('z')
        axis image
        title(' y')
        colorbar('vert')


        subplot('position',[0.65 0.15 0.33 0.33])
        imagesc(x,z,permute(abs(imageconfx(:,1,:)),[1 3 2])');
        axis xy

        shading interp
        xlabel('x')
        ylabel('z')
        axis image
        title(' z')
        colorbar('vert')

        uicontrol('Style', 'slider', 'Min',1,'Max', ny,...
      	  'val',1,'sliderstep',[1/(ny-1) 2/(ny-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotconfocaly,x,y,z,imageconf,imageconfx,imageconfy,imageconfz,nprint});
	printfig(nprint,'confocal')

    case 4


        figure(500)
        set(500,'DefaultAxesFontName','Times')
        set(500,'DefaultAxesFontSize',12)
        set(500,'DefaultAxesFontWeight','Bold')
        set(500,'DefaultTextfontName','Times')
        set(500,'DefaultTextfontSize',12)
        set(500,'DefaultTextfontWeight','Bold')
        set(500,'Position',[0 0 1000 600])

        uicontrol('Style', 'text', 'String', 'Choice of x','Position', [250 10 90 20]);
        uicontrol('Style', 'text', 'String', 'm','Position', [640 10 15 20]);
        uicontrol('Style', 'text', 'String', num2str(x(1),'%+1.2e\n'),'Position', [560 10 80 20]);

        subplot('position',[0.075 0.63 0.33 0.33])
        imagesc(y,z,permute(imageconf(1,:,:),[2 3 1])');
        axis xy

        shading interp
        xlabel('y')
        ylabel('z')
        axis image
        title('modulus')
        colorbar('vert')


        subplot('position',[0.65 0.63 0.33 0.33])
        imagesc(y,z,permute(abs(imageconfx(1,:,:)),[2 3 1])');
        axis xy

        shading interp
        xlabel('y')
        ylabel('z')
        axis image
        title(' x')
        colorbar('vert')

        subplot('position',[0.075 0.15 0.33 0.33])
        imagesc(y,z,permute(abs(imageconfy(1,:,:)),[2 3 1])');
        axis xy

        shading interp
        xlabel('y')
        ylabel('z')
        axis image
        title('y')
        colorbar('vert')


        subplot('position',[0.65 0.15 0.33 0.33])
        imagesc(y,z,permute(abs(imageconfz(1,:,:)),[2 3 1])');
        axis xy

        shading interp
        xlabel('y')
        ylabel('z')
        axis image
        title(' z')
        colorbar('vert')



        uicontrol('Style', 'slider', 'Min',1,'Max', nx,...
      	  'val',1,'sliderstep',[1/(nx-1) 2/(nx-1)],...
    	  'Position', [350 10 200 30],'Callback',{@plotconfocalx,x,y,z,imageconf,imageconfx,imageconfy,imageconfz,nprint});
	printfig(nprint,'confocal')

end

