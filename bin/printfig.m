function printfig(nprint,filename)

  if (nprint(1,1) == 'y')    
    L = strlength(nprint(2,:));
    if (nprint(3,1) == 'y')
      F = getframe(gcf);
      imwrite(F.cdata,strcat(filename,'.',nprint(2,1:L)))
    else
      resdpi=300;
      exportgraphics(gcf,strcat(filename,'.',nprint(2,1:L)),'Resolution',resdpi)
    end
  end
